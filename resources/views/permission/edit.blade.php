@extends('layouts.master')
@section('title','تصحیح صلاحیت')
@section('content')
    <form class="m-form m-form--fit m-form--label-align-right" id="permissions" method="POST" enctype="multipart/form-data" action="{{route('permissions.update',$permission->id)}}">
        @csrf
        @method('put')
        <div class="m-portlet__body">
        <div class="form-group row ">
            <input type="hidden" value="web" name="guard_name">
            <div class="form-group col m-form__group {{$errors->has('name_dr') ? 'has-danger' : ''}}">
            <label for="name_dr">نام دری </label>
                <input type="text" name="name_dr" value="{{$permission->name_dr}}" class="form-control m-input m-input--square" id="name_dr" placeholder="نام دری ">
                @if($errors->has('name_dr'))
                    <div class="form-control-feedback">{{$errors->first('name_dr')}}</div>
                @endif
            </div>
        </div>
        
        <div class="form-group row ">
            <div class="form-group col m-form__group {{$errors->has('name') ? 'has-danger' : ''}}">
            <label for="name">نام انگلیسی </label>
                <input type="text" name="name" value="{{$permission->name}}" class="form-control m-input m-input--square" id="name" placeholder="نام انگلیسی ">
                @if($errors->has('name'))
                    <div class="form-control-feedback">{{$errors->first('name')}}</div>
                @endif
            </div>
        </div>
            
        
            <div class="form-group row ">
                <div class="form-group col-md-6 m-form__group ">
                    <button class="btn btn-success">تصحیح صلاحیت</button>
                </div>
            </div>
        </div>

    </form>

@endsection
