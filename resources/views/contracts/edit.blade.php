@extends('layouts.master')
@section('title', trans('global.add_contract'))
@section('content')
<style>
    .m-form.m-form--group-seperator-dashed .m-form__group{
        border-bottom: none;
        padding:10px;
    }
    .m-portlet .m-portlet__foot:not(.m-portlet__no-border){
        border:none;
    }
</style>
<div id="contract">
    <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" enctype="multipart/form" v-if="!Object.keys(contract_info).length" method="POST"  action="{{route('contracts.update',$contract->id)}}">
        @csrf
        @method('put')
        <div class="m-portlet__body">
            <div class="form-group m-form__group row ">
                <div class="col-lg-6 {{$errors->has('contract_type_id') ? 'has-danger' : ''}}">
                    <label>{{trans('global.contract_type')}}</label>
                    <select class="form-control" name="contract_type_id" >
                        <option selected disabled>{{trans('global.select_contranct_type')}}</option>
                        @foreach($contract_types as $contract_type)
                            <option {{$contract->contract_type_id==$contract_type->id ? 'selected': '' }} value="{{$contract_type->id}}"> {{$contract_type->name_dr}}</option>
                        @endforeach
                    </select>
                    @if($errors->has('contract_type_id'))
                        <div class="form-control-feedback">{{$errors->first('contract_type_id')}}</div>
                    @endif    
                </div>
                <div class="col-lg-6 {{$errors->has('identification_number') ? 'has-danger' : ''}}">
                    <label>{{trans('global.identification_number')}}</label>
                    <input type="text" name="identification_number"  value="{{$contract->identification_number}}" class="form-control m-input" placeholder="{{trans('global.identification_number')}}">
                    @if($errors->has('identification_number'))
                        <div class="form-control-feedback">{{$errors->first('identification_number')}}</div>
                    @endif      
            </div>
            </div>
            <div class="form-group m-form__group row ">
                <div class="col-lg-6 {{$errors->has('department_id') ? 'has-danger' : ''}}">
                    <label>{{trans('global.department')}}</label>
                    <select name="department_id"  class="form-control">
                        <option selected disabled> {{trans('global.department')}}</option>

                        @foreach ($departments as $item)
                            <option {{$contract->department_id== $item->id ? ' selected' : ''}} value="{{$item->id}}">{{$item->name_dr}}</option>
                        @endforeach
                    </select>
                    @if($errors->has('department_id'))
                        <div class="form-control-feedback">{{$errors->first('department_id')}}</div>
                    @endif    
                </div>
                <div class="col-lg-6 {{$errors->has('contractor_company_id') ? 'has-danger' : ''}}">
                    <label>{{trans('global.contractor_company')}}</label>
                    <select class="form-control" name="contractor_company_id" >
                        <option selected disabled> {{trans('global.contractor_company')}}</option>

                        @foreach ($contractor_companies as $item)
                            <option {{$contract->contractor_company_id == $item->id ? 'selected' :''}} value="{{$item->id}}"> {{$item->company_name}}</option>
                        @endforeach
                    </select>
                    @if($errors->has('contractor_company_id'))
                        <div class="form-control-feedback">{{$errors->first('contractor_company_id')}}</div>
                    @endif    
                </div>
            </div>
            <div class="form-group m-form__group row ">    
                <div class="form-group col-md-6 {{$errors->has('year') ? 'has-danger' : ''}}">
                    <label for="inputPassword4">{{trans('global.year')}}</label>
                    <div class="input-group year">
                        <select class="form-control" name="year" >
                            <option selected disabled>{{trans('global.year')}}</option>
                            @for($i = 1390; $i <= 1410; $i++)
                            <option {{$contract->year == $i ? 'selected' : ''}} value="{{$i}}">{{$i}}</option>
                            @endfor
                        </select>
                        <div class="input-group-append">
                            <span class="input-group-text">
                                <i class="la la-calendar"></i>
                            </span>
                        </div>
                    </div>
                    @if($errors->has('year'))
                    <div class="form-control-feedback">{{$errors->first('year')}}</div>
                    @endif
                </div>
                <div class="form-group col-md-6 {{$errors->has('contract_amount') ? 'has-danger' : ''}}">
                    <label for="inputPassword4">{{trans('global.contract_amount')}}</label>
                    <div class="input-group ">
                        <input type="number" name="contract_amount" id="contract_amount" value="{{$contract->contract_amount}}" class="form-control m-input" placeholder="{{trans('global.contract_amount')}}">
                    </div>
                    @if($errors->has('contract_amount'))
                    <div class="form-control-feedback">{{$errors->first('contract_amount')}}</div>
                    @endif
                </div>
            </div>
            <div class="form-group m-form__group row ">
                <div class="form-group col-md-6 {{$errors->has('contract_discount_perce') ? 'has-danger' : ''}}">
                    <label for="inputPassword4">{{trans('global.contract_discount_perce')}}%</label>
                    <div class="input-group ">
                        <input type="number" name="contract_discount_perce" onkeyup="calculate()" step="0.1" min="0" max="100" id="contract_discount_perce" value="{{$contract->contract_discount_perce}}" class="form-control m-input" placeholder="{{trans('global.contract_discount_perce')}}%">
                    </div>
                    @if($errors->has('contract_discount_perce'))
                    <div class="form-control-feedback">{{$errors->first('contract_discount_perce')}}</div>
                    @endif
                </div>
                <div class="form-group col-md-6 {{$errors->has('contract_discount') ? 'has-danger' : ''}}">
                    <label for="inputPassword4">{{trans('global.contract_discount')}}</label>
                    <div class="input-group ">
                        <input type="number" id="contract_discount" name="contract_discount" value="" step="0.1"   value="{{$contract->contract_discount}}" class="form-control m-input" placeholder="{{trans('global.contract_discount')}}">
                    </div>
                   
                </div>
            </div>
            <div class="form-group m-form__group row ">
                <div class="form-group col-md-6 {{$errors->has('start_date') ? 'has-danger' : ''}}">
                    <label for="inputPassword4">{{trans('global.start_date')}}</label>
                    
                    {{-- <date-picker
                        
                        locale="fa" format="jYYYY-jMM-jDD" required>
                    </date-picker> --}}
                    <input type="text" name="start_date"  value="{{$contract->start_date}}" class="form-control persian_date" placeholder="تاریخ انتخاب کنید.">
                    
                    
                    
                    @if($errors->has('start_date'))
                    <div class="form-control-feedback">{{$errors->first('start_date')}}</div>
                    @endif
                </div>
                
                <div class="form-group col-md-6 {{$errors->has('end_date') ? 'has-danger' : ''}}">
                    <label for="inputPassword4">{{trans('global.end_date')}}</label>
                    {{-- <date-picker
                        
                        locale="fa" format="jYYYY-jMM-jDD" required>
                    </date-picker> --}}
                    <input type="text" name="end_date"  value="{{$contract->end_date}}" class="form-control persian_date" placeholder="تاریخ انتخاب کنید.">
                    
                    @if($errors->has('end_date'))
                    <div class="form-control-feedback">{{$errors->first('end_date')}}</div>
                    @endif
                </div>
            </div>
            
            <div class="form-group m-form__group row "> 
                
                    <div class="form-group col m-form__group pt-0 {{$errors->has('contract_file') ? 'has-danger' : ''}}">
                            <label for="exampleInputEmail1">{{trans('global.contract_file')}}</label>
                            <div></div>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="image" id="customFile">
                                <label class="custom-file-label" for="customFile">فایل را انتخاب نمائید</label>
                            </div>
                        @if($errors->has('contract_file'))
                            <div class="form-control-feedback">{{$errors->first('contract_file')}}</div>
                        @endif
                    </div>

           
            </div>   

            <div class="m-portlet__foot m-portlet__foot--fit">
                <div class="m-form__actions">
                    <button type="submit" class="btn btn-primary">{{trans('global.submit')}}</button>
                    <button type="reset" class="btn btn-danger">{{trans('global.cancel')}}</button>
                </div>
            </div>
        </div>

    </form>
</div>

@endsection
@push('custom-js')

<script>
function calculate(){
    contract_amount = $('#contract_amount').val();
    contract_discount_perce = $('#contract_discount_perce').val();
    

    if(contract_amount>0 && contract_discount_perce > 0){
        contract_discount = (contract_amount * contract_discount_perce)/100;
        $('#contract_discount').val(contract_discount);
    }
}
</script>
</script>
@endpush
