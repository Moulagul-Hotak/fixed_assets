@extends('layouts.master')
@section('title',trans('global.item_report'))
@section('content')
<div id="report_page">
<form class="" id="report_form" @submit.prevent="getReport">
    @csrf
    <div class="row">

        <div class="col-md-3">
        <div class="form-group">
                <label for="">{{trans('global.main_category')}}</label> 
                {{-- <select class="form-control itme_details" name="MainCategory" id="main_category">
                    <option value="">{{trans('global.main_category')}}</option>
                </select>   --}}

            ‍‍‍   <v-select
                                                
                label="name_dr" 
                :options="items"
                item-text=""
                dir="rtl"
                v-model="main_category_id"
                :reduce="item=> item.id"
                @search="(search, loading) => onSearch(search, loading, 'MainCategory')"
                                                    
            >
                <template slot="no-options">
                جستجو       
                </template>
                <template slot="option" slot-scope="option">
                        @{{  option.name_dr }}     
                </template>
            </v-select>
            </div> 
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="">{{trans('global.sub_category')}}</label> 
                {{-- <select class="form-control itme_details" name="SubCategory" id="sub_category">
                    <option value="">{{trans('global.sub_category')}}</option>
                </select>   --}}

                <v-select
                                                
                    label="name_dr" 
                    :options="items"
                    item-text=""
                    dir="rtl"
                    v-model="sub_category_id"
                    :reduce="item=> item.id"
                    @search="(search, loading) => onSearch(search, loading, 'SubCategory')"
                                                    
                >
                    <template slot="no-options">
                    جستجو       
                    </template>
                    <template slot="option" slot-scope="option">
                            @{{  option.name_dr }}     
                    </template>
                </v-select>
            </div> 
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="">{{trans('global.end_category')}}</label> 
                {{-- <select class="form-control itme_details" name="EndCategory" id="end_category">
                    <option value="">{{trans('global.end_category')}}</option>
                </select>   --}}
                <v-select
                                                    
                    label="name_dr" 
                    :options="items"
                    item-text=""
                    dir="rtl"
                    v-model="end_category_id"
                    :reduce="item=> item.id"
                    @search="(search, loading) => onSearch(search, loading, 'EndCategory')"
                                                    
                >
                    <template slot="no-options">
                    جستجو       
                    </template>
                    <template slot="option" slot-scope="option">
                            @{{  option.name_dr }}     
                    </template>
                </v-select>
            </div> 
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for=""> اسم جنس</label> 
                {{-- <select class="form-control itme_details" name="ItemDetails" id="itme_details">
                    <option value="">{{trans('global.item_details_index')}}</option>
                </select>   --}}

                <v-select
                                                    
                    label="name_dr" 
                    :options="items"
                    item-text=""
                    dir="rtl"
                    v-model="item_details_id"
                    :reduce="item=> item.id"
                    @search="(search, loading) => onSearch(search, loading, 'ItemDetails')"
                                                
                >
                    <template slot="no-options">
                    جستجو       
                    </template>
                    <template slot="option" slot-scope="option">
                            @{{  option.name_dr }}     
                    </template>
                </v-select>
            </div> 
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label for="">{{trans('global.department')}}</label> 
                {{-- <select class="form-control itme_details" name="Department" id="department">
                    <option value="">{{trans('global.department')}}</option>
                </select>   --}}

                <v-select
                                                        
                    label="name_dr" 
                    :options="items"
                    item-text=""
                    dir="rtl"
                    v-model="department_id"
                    :reduce="item=> item.id"
                    @search="(search, loading) => onSearch(search, loading, 'Department')"
                                                
                >
                    <template slot="no-options">
                    جستجو       
                    </template>
                    <template slot="option" slot-scope="option">
                            @{{  option.name_dr }}     
                    </template>
                </v-select>
            </div> 
        </div>
        <div class="col-md-5">
            <div class="form-group">
                <label for="">{{trans('global.user')}}</label> 
                {{-- <select class="form-control itme_details" name="Employee" id="employee">
                    <option value="">{{trans('global.user')}}</option>
                </select>   --}}

                <v-select
                                                    
                    label="name_dr" 
                    :options="items"
                    item-text=""
                    dir="rtl"
                    v-model="employee_id"
                    :reduce="item=> item.id"
                    @search="(search, loading) => onSearch(search, loading, 'Employee')"
                                                
                >
                    <template slot="no-options">
                    جستجو       
                    </template>
                    <template slot="option" slot-scope="option">
                            @{{  option.name_dr }}     
                            (    
                                @{{option.last_name}}
                            )
                            ولد -- 
                            @{{option.father_name_dr}}
                            
                            وظیفه -- 
                            @{{option.current_position_dr}}
                            
                    </template>
                </v-select>
            </div> 
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="">{{trans('global.user')}}</label> 
                {{-- <select class="form-control itme_details" name="Employee" id="employee">
                    <option value="">{{trans('global.user')}}</option>
                </select>   --}}

                <v-select
                                                    
                    label="email" 
                    :options="users"
                    item-text=""
                    dir="rtl"
                    v-model="user_id"
                    :reduce="user=> user.id"                          
                >

              
                </v-select>
            </div> 
        </div>
    </div>
    <div class="row">
        <div class="form-group" v-if="print">
            <button type="submit" class="btn btn-primary form-control">{{trans('global.search')}}</button>
        </div> 
    </div>
</form>
<div class="row" v-if="show" id="print_btn">
    
    <button class="btn btn-primary" OnClick="printFacture()" type="button" >پرنت <i class="fa fa-print"> </i></button>
    <button class="btn btn-success" onClick="export_report('excel')"  type="button">اکسل <i class="fa fa-file-excel-o"> </i></button>
    <button class="btn btn-danger" onClick="export_report('pdf')" type="button">پی دی اف <i class="fa fa-file-pdf-o" aria-hidden="true"></i></button>

</div>

    <div class="m-blockui loader" v-if="loader"><span>لطفا منتظر باشید </span><span><div class="m-loader  m-loader--brand m-loader--lg"></div></span></div>

<table id="printable" v-if="show" class="table table-striped table-bordered jquery-datatable" style="width:100%">
    <thead>
        <tr>
            <th>#</th>
            <th>استفاده کننده</th>
            <th>ولد</th>
            <th>مشخصات جنس</th>
            <th>کمپنی</th>
            <th>سریال نمبر</th>
            <th>تعداد</th>
            <th>واحد</th>
            <th>فیات</th>
            <th>اداره</th>
            <th>جمله قیمت</th>
            <th>تاریخ توزیع</th>
            <th> توزیع کننده</th>
        </tr>
    </thead>
    <tbody>
    

        <tr v-for="(item,index) in reports">
            <td>@{{index+1}}</td>
            <td>@{{item?.item_details_specifications?.motamed?.employee?.name_dr}}</td>
            <td>@{{item?.item_details_specifications?.motamed?.employee?.father_name_dr}}</td>
            <td>@{{item?.item_details_specifications?.item_details?.name_dr}}</td>
            <td>@{{item?.item_details_specifications?.item_details?.vendor?.name_dr}}</td>
            <td>@{{item?.item_details_specifications?.col1}} || @{{item?.item_details_specifications?.col2}} || @{{item?.item_details_specifications?.col3}} || @{{item?.item_details_specifications?.col4}} ||
                @{{item?.item_details_specifications?.col5}} || @{{item?.item_details_specifications?.col6}} || @{{item?.item_details_specifications?.col7}} || @{{item?.item_details_specifications?.col8}} ||
                @{{item?.item_details_specifications?.col9}} || @{{item?.item_details_specifications?.col10}} @{{item?.item_details_specifications?.col11}} || @{{item?.item_details_specifications?.col12}}
            </td>
            <td>1</td>
            <td>@{{item?.item_details_specifications?.item_details?.unit?.name_dr}}</td>
            <td>@{{item?.item_details_specifications?.price}} || @{{item?.item_details_specifications?.currency==0 ? 'افغانی' : 'دالر'}}</td>
            <td>
                @{{item?.item_details_specifications?.motamed?.employee?.department?.name_dr}}
                (@{{item?.item_details_specifications?.motamed?.employee?.current_position_dr}})
            </td>
            <td>@{{item?.item_details_specifications?.price}} || @{{item?.item_details_specifications?.currency==0 ? 'افغانی' : 'دالر'}}</td>
            <td>@{{item?.item_details_specifications?.motamed?.allotment_date}}</td>
            <td>@{{item?.item_details_specifications?.created_by?.employee?.name_dr}}</td>
        </tr>
    
</table>
<form method="post" action="" id="report_export">
    @csrf
    <input type="hidden" name="report_type" id="report_type">
    <input type="hidden" name="main_category_id" v-model="main_category_id">
    <input type="hidden" name="sub_category_id" v-model="sub_category_id">
    <input type="hidden" name="end_category_id" v-model="end_category_id">
    <input type="hidden" name="item_details_id" v-model="item_details_id">
    <input type="hidden" name="department_id" v-model="department_id">
    <input type="hidden" name="employee_id" v-model="employee_id">
    <input type="hidden" name="type" value="allotment">

</form>
</div>
@endsection
@push('custom-js')

<script>



$(document).ready(function(){

 
    $(".itme_details").select2({
       
        ajax: { 
        url: '{{route("select2_search_ajax")}}',
        type:"post",
        dataType: 'json',
        delay: 250,
        data: function (params) {
        return {
            searchTerm: params.term,
            table_name: $(this).attr('name'),
            main_category_id: $('#main_category').val(), 
            sub_category_id: $('#sub_category').val(), 
            end_category_id: $('#end_category').val(), 
        };
        },
        processResults: function (response) {
            return {
            results: response
            };
        },
        cache: true
        }
    });
});

</script>


<script>

    Vue.component('v-select', VueSelect.VueSelect);

    item_app = new Vue({
        el: '#report_page',
        data: {
    
            items:[],
            main_category_id :'',
            sub_category_id :'',
            end_category_id :'',
            item_details_id :'',
            department_id   :'',
            employee_id :'',
            current_model:'',
            reports:[],
            users:{!!$users!!},
            show:false,
            print:true,
            loader:false,
            user_id:'',
 


         
        },

        methods: {
      

            onSearch(search, loading,model) {
                this.current_model = model;
                loading(true);
                this.search(loading, search, this,this.current_model);
                console.log(model);
                },
            search: _.debounce((loading, search, vm,current_model) => {

                axios.post(`{{route("item_axios")}}`,{
                            'name' : search,
                            'model' : current_model,
                            
                            })           
                    .then(res => {
                        vm.items = res.data.items;                     
                        loading(false);
                    })
                    .catch(err => {
                        console.log(err);
                    });
                    
                }, 350),

                searchItem(){
                    this.getAllItemsForSearch();
                    search_by = this.search_type;
                    filter_data = this.search_items.filter(item => {
                        if(item[search_by]) {
                            return item[search_by].toLowerCase().indexOf(this.search_text.toLowerCase()) > -1
                        }
                });

                this.items = filter_data;
               
                },
                setModel(model){
                    this.current_model = model;
                },
                getReport(event) {
                    this.loader = true;
                   
                    axios.post("{{route('item_report.store')}}", {
                        
                        'main_category_id' : this.main_category_id,
                        'sub_category_id' : this.sub_category_id,
                        'end_category_id' : this.end_category_id,
                        'item_details_id' : this.item_details_id,
                        'department_id' : this.department_id  ,  
                        'employee_id' : this.employee_id,
                        'type' : 'allotment',
                        'user_id' : this.user_id,
                    })
                    .then(res => {
                        this.loader = false;
                        this.reports = res.data
                        this.show = true
                    })
                    .catch(err => {
                        console.log(err);
                    });
                },     
               
            
        },
        mounted() {

        },
    });

    function printFacture(){
        $('#print_btn').hide();
        print();
        $('#print_btn').show();
    
    }

    function export_report(report_type){

        if(report_type =='excel'){
            var route = "{{route('export_excel')}}";
            $('#report_export').attr('action', route);
            $('#report_type').val(report_type);

        }else{
            var route = "{{route('export_excel')}}";
            $('#report_export').attr('action', route);
            $('#report_type').val(report_type);
        }

        $('#report_export').submit();
    }


</script>
@endpush

@push('custom-css')
<style>
    .loader{
        position: absolute;
        left: 45%;
    }
</style>
@endpush
