<tr data-filter-item
                                        data-filter-name="{{ $item->itemDetailsSpecifications->itemDetails['name_dr'] }}"
                                        data-filter-item-details data-filter-details="{{ $item->details }}">
                                        <td class="un">
                                            <div class="m-checkbox-list">
                                                <label class="m-checkbox" style="display:inline;margin:1.5em;">
                                                    @php

                                                        if ($item->itemBalance) {
                                                            $item_amount = $item->itemBalance->item_amount;
                                                        } else {
                                                            $item_amount = 1;
                                                        }

                                                    @endphp
                                                    {{-- {{$item->itemDetailsSpecifications->subCategoryKeys->distribution_type ? $item->itemBalance->item_amount : 1}})"> --}}

                                                    <input type="checkbox"
                                                        {{ $item->itemDetailsSpecifications->condition == '2' || $item->itemDetailsSpecifications->condition == '3' ? 'disabled' : '' }}
                                                        {{ $item->status != 1 ? 'disabled' : '' }}
                                                        id="{{ $index }}" class="checkbox"
                                                        @change="getItemDetailsSpecifications(
                                                            $event.target.checked,
                                                            {{ $index }},
                                                            {{ $item->item_details_specifications_id }},
                                                            {{ $item->itemDetailsSpecifications->subCategoryKeys->distribution_type ? $item_amount : 1 }})">
                                                    <span></span>
                                                </label>
                                            </div>
                                        </td>
                                        <td class="text-center">{{ ++$index }}</td>
                                        <td>{{ $item->itemDetailsSpecifications->itemDetails['name_dr'] }}</td>
                                        <td>{{ $item->itemDetailsSpecifications->itemDetails->unit->name_dr }}</td>
                                        <td>
                                            @if ($item->itemDetailsSpecifications->subCategoryKeys->distribution_type)
                                                {{ $item_amount }}
                                            @else
                                                1
                                            @endif
                                        </td>
                                        <td>
                                            <div class="details">
                                                @if ($item->itemDetailsSpecifications->itemDetails->description)
                                                    <span class="m--font-primary">
                                                        {{ $item->itemDetailsSpecifications->itemDetails->description }} :
                                                    </span>
                                                    <span class="m--font-info">
                                                        {{ $item->itemDetailsSpecifications->details }} ||
                                                    </span>
                                                @endif
                                                @if ($item->itemDetailsSpecifications->subCategoryKeys->col1)
                                                    <span class="m--font-primary">
                                                        {{ $item->itemDetailsSpecifications->subCategoryKeys->col1 }} :
                                                    </span>
                                                    <span class="m--font-info">
                                                        {{ $item->itemDetailsSpecifications->col1 }} ||
                                                    </span>
                                                @endif
                                                @if ($item->itemDetailsSpecifications->subCategoryKeys->col2)
                                                    <span class="m--font-primary">
                                                        {{ $item->itemDetailsSpecifications->subCategoryKeys->col2 }} :
                                                    </span>
                                                    <span class="m--font-info">
                                                        {{ $item->itemDetailsSpecifications->col2 }} ||
                                                    </span>
                                                @endif

                                                @if ($item->itemDetailsSpecifications->subCategoryKeys->col3)
                                                    <span class="m--font-primary">
                                                        {{ $item->itemDetailsSpecifications->subCategoryKeys->col3 }} :
                                                    </span>
                                                    <span class="m--font-info">
                                                        {{ $item->itemDetailsSpecifications->col3 }} ||
                                                    </span>
                                                @endif
                                                <br>
                                                @if ($item->itemDetailsSpecifications->subCategoryKeys->col4)
                                                    <span class="m--font-primary">
                                                        {{ $item->itemDetailsSpecifications->subCategoryKeys->col4 }} :
                                                    </span>
                                                    <span class="m--font-info">
                                                        {{ $item->itemDetailsSpecifications->col4 }}
                                                    </span>
                                                @endif
                                                @if ($item->itemDetailsSpecifications->subCategoryKeys->col5)
                                                    <span class="m--font-primary">
                                                        {{ $item->itemDetailsSpecifications->subCategoryKeys->col5 }} :
                                                    </span>
                                                    <span class="m--font-info">
                                                        {{ $item->itemDetailsSpecifications->col5 }}
                                                    </span>
                                                @endif
                                                @if ($item->itemDetailsSpecifications->subCategoryKeys->col6)
                                                    <span class="m--font-primary">
                                                        {{ $item->itemDetailsSpecifications->subCategoryKeys->col6 }} :
                                                    </span>
                                                    <span class="m--font-info">
                                                        {{ $item->itemDetailsSpecifications->col6 }}
                                                    </span>
                                                @endif
                                                @if ($item->itemDetailsSpecifications->subCategoryKeys->col7)
                                                    <span class="m--font-primary">
                                                        {{ $item->itemDetailsSpecifications->subCategoryKeys->col7 }} :
                                                    </span>
                                                    <span class="m--font-info">
                                                        {{ $item->itemDetailsSpecifications->col7 }}
                                                    </span>
                                                @endif
                                                @if ($item->itemDetailsSpecifications->subCategoryKeys->col8)
                                                    <span class="m--font-primary">
                                                        {{ $item->itemDetailsSpecifications->subCategoryKeys->col8 }} :
                                                    </span>
                                                    <span class="m--font-info">
                                                        {{ $item->itemDetailsSpecifications->col8 }}
                                                    </span>
                                                @endif
                                                @if ($item->itemDetailsSpecifications->subCategoryKeys->col9)
                                                    <span class="m--font-primary">
                                                        {{ $item->itemDetailsSpecifications->subCategoryKeys->col9 }} :
                                                    </span>
                                                    <span class="m--font-info">
                                                        {{ $item->itemDetailsSpecifications->col9 }}
                                                    </span>
                                                @endif
                                                @if ($item->itemDetailsSpecifications->subCategoryKeys->col10)
                                                    <span class="m--font-primary">
                                                        {{ $item->itemDetailsSpecifications->subCategoryKeys->col10 }} :
                                                    </span>
                                                    <span class="m--font-info">
                                                        {{ $item->itemDetailsSpecifications->col10 }}
                                                    </span>
                                                @endif
                                                @if ($item->itemDetailsSpecifications->subCategoryKeys->col11)
                                                    <span class="m--font-primary">
                                                        {{ $item->itemDetailsSpecifications->subCategoryKeys->col11 }} :
                                                    </span>
                                                    <span class="m--font-info">
                                                        {{ $item->itemDetailsSpecifications->col11 }}
                                                    </span>
                                                @endif
                                                @if ($item->itemDetailsSpecifications->subCategoryKeys->col12)
                                                    <span class="m--font-primary">
                                                        {{ $item->itemDetailsSpecifications->subCategoryKeys->col12 }} :
                                                    </span>
                                                    <span class="m--font-info">
                                                        {{ $item->itemDetailsSpecifications->col12 }}
                                                    </span>
                                                @endif
                                                ||
                                                <span class="m--font-primary">
                                                    قیمت :
                                                </span>
                                                <span class="m--font-info">

                                                    {{ $item->itemDetailsSpecifications->price }}
                                                    {{ $item->itemDetailsSpecifications->currency == 0 ? 'افغانی' : 'دالر' }}
                                                </span>
                                                ||

                                                @if ($item->itemDetailsSpecifications->subCategoryKeys->distribution_type)
                                                    <span class="m--font-primary">
                                                        قیمت مجموعی :
                                                        {{-- {{($item->itemDetailsSpecifications->price) * ($item->itemBalance->item_amount) }} --}}
                                                        {{ $item->itemDetailsSpecifications->currency == 0 ? 'افغانی' : 'دالر' }}

                                                    </span>
                                                @endif

                                                @if ($item->status == 1)
                                                    <a href="#" data-toggle="modal" data-target="#editModal"
                                                        title="تصحیح جنس"
                                                        @click="editItemDetailsSpecifications({{ $item->item_details_specifications_id }})">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                @endif
                                            </div>
                                            <a href="javascript:;" class="btn btn-xs green toggle_description">
                                                نمایش <i class="fa fa-plus"></i>
                                            </a>
                                        </td>

                                        <td>
                                            {{ $item->allotment_date }}
                                        </td>

                                        <td>
                                            {{ $item->details }}
                                        </td>
                                        <td>
                                            {{ $item->return_date }}
                                        </td>
                                        <td>
                                            @if ($item->status == 1)
                                                <i class="la la-check m--font-success"></i>
                                            @elseif($item->status == 2)
                                                <i class="la la-home m--font-warning"></i>
                                            @else
                                                <i class="la la-close m--font-danger"></i>
                                            @endif
                                        </td>
                                        <td>
                                            @if ($item->itemDetailsSpecifications->condition == 0)
                                                <span class="m--font-success p-2">
                                                    جدید
                                                </span>
                                            @elseif($item->itemDetailsSpecifications->condition == 1)
                                                <span class="m--font-warning p-2">
                                                    مستعمل
                                                </span>
                                            @elseif($item->itemDetailsSpecifications->condition == 3)
                                                <span class="m--font-warning p-2">
                                                    غیر فعال
                                                </span>
                                            @else
                                                <span class="m--font-danger p-2">
                                                    داغمه
                                                </span>
                                            @endif
                                        </td>
                                        <td class="un">
                                            {{-- <a href="#" data-toggle="modal"  data-target="#allotment_history" title="" >
                                                    <i class="la la-rotate-left"></i>
                                                </a> --}}

                                            @php

                                                $item_balance_type = $item->itemDetailsSpecifications->ItemBalance->where('type', '0')->count();
                                                if ($item_balance_type) {
                                                    // $item_balance_type = $item_balance_type->first()->id;
                                                    $item_balance_type = 1;
                                                } else {
                                                    $item_balance_type = 'null';
                                                }

                                            @endphp
                                            <a href="#" data-toggle="modal" data-target="#allotment_history"
                                                title="تاریخچه جمع و قید جنس"
                                                @click="showItemAllotmentHistory(
                                                    {{ $item->item_details_specifications_id }},
                                                    {{ $item->itemDetailsSpecifications->subCategoryKeys->distribution_type ? $item_balance_type : 'null' }}
                                                    {{-- this is old code  --}}
                                                    {{-- $item->itemDetailsSpecifications->subCategoryKeys->distribution_type ? $item->itemDetailsSpecifications->ItemBalance->where('type','0')->first()->id : 'null' --}}
                                                    )
                                                    ">



                                                <i class="la la-list-alt"></i>
                                            </a>
                                            @if ($item->status == 1)
                                                <a href="#" data-toggle="modal" data-target="#return_allotment"
                                                    title="برگشت به دیپو"
                                                    @click="editItemAllotment({{ $item->item_details_specifications_id }})">
                                                    <i class="fa fa-undo"></i>
                                                </a>
                                                @if ($item->status == 1)
                                                    <a href="#" data-toggle="modal" data-target="#editItemModal"
                                                        title="تصحیح جمع و قید" @click="editItem({{ $item->id }})">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                @endif
                                            @endif
                                        </td>
                                    </tr>


                                  