@extends('layouts.master')
@section('title','صفحه اصلی')
@section('content')
<div id="printableArea">
    <div class="row">
        <div class="col-md-12 text-center text-dark">
            <h4>فورم ف س (5)</h4><br>
            <h5>تکت توزیع تحویلخانه</h5>
        </div>
    </div>
    <br>
    <div class="row mb-3">
        <table class="table">
            <thead>
                <tr>
                    <th>1-	</th>
                    <th colspan="3">نمبر تکت توزیع: {{$fece5->number}}</th>
                    <th>4-	</th>
                    <th colspan="2">به اساس درخواست نمبر ({{$fece5->fece9->number}})</th>
                </tr>
                <tr>
                    <th>2-	</th>
                    <th colspan="3">تاریخ:‌ {{$fece5->date}}</th>
                    <th>5-	</th>
                    <th colspan="2">تاریخ درخواستی: {{$fece5->fece9->date}}</th>
                </tr>
                <tr>
                    <th>3-	</th>
                    <th colspan="3">تحویلخانه توزیع کننده:	{{$fece5->distributerEmployee->name_dr}} -- ولد : {{$fece5->distributerEmployee->father_name_dr}}</th>
                    <th>6-	</th>
                    <th colspan="2">شعبه درخواست کننده: {{$fece5->fece9->department->name_dr}}</th>
                </tr>
                <tr class="with_background">
                    <th>شماره</th>
                    <th>مقدار	</th>
                    <th>واحد	</th>
                    <th>تفصیلات	</th>
                    <th>قیمت واحد	</th>
                    <th>قیمت مجموعی	</th>
                    <th>معامله شد بحساب</th>
                </tr>
                <tr class="with_background">
                    <th>7</th>
                    <th>8</th>
                    <th>9</th>
                    <th>10</th>
                    <th>11</th>
                    <th>12</th>
                    <th>13</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $total = 0;
                    $counter = 0;
                @endphp
                @if($fece5->fece9->fece9_type == 0)
                @foreach ($fece5->itemDetails->groupBy('name_dr') as $index => $item)
                    @foreach ($item as $number => $details)
                        <tr>
                            <td>{{++$number}}</td>
                            <td>1</td>
                            <td>{{$details->unit->name_dr}}</td>
                            {{-- temporarily get the employees name manually + have to work on relationship --}}
                            <td>{{$index}} < ---- > @php $receiver_employee = App\Employee::find($details->pivot->receiver_employee_id);@endphp {{$receiver_employee->name_dr}} -- ولد : {{$receiver_employee->father_name_dr}}</td>
                            <td>
                                    {{$contract_item_details[0]->price_per_item}}
                            </td>
                                <td>
                                    {{$sub_total = ($contract_item_details[0]->price_per_item * 1)}}
                                </td>
                                @php
                                $sub_total = ($contract_item_details[0]->price_per_item * 1);
                                $total += (float)$sub_total;
                                $counter += 1;
                                @endphp

                        <td></td>
                        </tr>
                        @endforeach
                    @endforeach
                @else
                    @foreach ($fece5->itemDetails->groupBy('name_dr') as $index => $item)
                        @foreach ($item as $number => $details)
                            <tr>
                                <td>{{++$number}}</td>
                                <td>1</td>
                                <td>{{$details->unit->name_dr}}</td>
                                {{-- temporarily get the employees name manually + have to work on relationship --}}
                                <td>{{$index}} < ---- > @php $receiver_employee = App\Employee::find($details->pivot->receiver_employee_id);@endphp {{$receiver_employee->name_dr}} -- ولد : {{$receiver_employee->father_name_dr}}</td>
                                <td>
                                        {{$details->itemDetailsSpecifications->first()->price}}

                                        {{$sub_total = ($details->itemDetailsSpecifications->first()->price * 1)}}
                                    </td>
                                    <td>
                                    </td>
                                    @php
                                    $total += (float)$sub_total;
                                    $counter += 1;
                                    @endphp

                            <td></td>
                            </tr>
                            @endforeach
                        @endforeach
                @endif
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>جمله شد</td>
                    <td>
                        {{$total}}
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>
                            قرارشـرح فوق {{$counter}} قلم جـــنس متـــذکره به اســــاس درخواســـت نمبر فوق که دارای امــریه مقام بوده در وجه اشخاص متذکره حواله و اسناد غرض ویزه بمدیریت محترم کنترول ارسال است. با احترام
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>

            </tbody>
            <tfoot>
                <tr>
                    <td colspan="3">14</td>
                    <td colspan="2">15</td>
                    <td colspan="2">16</td>
                </tr>
                <tr>
                    <td colspan="3">امضاء</td>
                    <td colspan="2">کاتب اندراج</td>
                    <td colspan="2">امضاء گیرنده</td>
                </tr>
                <tr>
                    <td colspan="7">
                        <ul>
                            <li>
                                توزیع فورمه در یک اصل و دو کاپی ترتیب شود
                                <ol>
                                    <li>اصل به درخواست کننده داده شود</li>
                                    <li>کاپی اول به شعبه جنسی</li>
                                    <li>کاپی دوم به شعبه تحویلخانه</li>
                                </ol>
                            </li>
                        </ul>
                    </td>
                </tr>
            </tfoot>
        </table>
        @if($fece5->processed_fece5_file)
            <div class="row no-print">
                <div class="col-md-3 mb-2 pl-0">
                    <div class="update-image-wrapper">
                        <img class="w-100px" src="{{asset('public/images/pdf-2.png')}}" alt="" />

                        <div class="overlay">
                            <div class="text">
                                <a href="{{asset($fece5->processed_fece5_file)}}" data-fancybox data-caption="{{$fece5->description}}">
                                    نمایش
                                </a>
                                <a href="javascript:void(0)" onclick="$('input[name=image]').click();">
                                        تصحیح
                                </a>


                                <form action="{{route('fece5s.replace_attached_file')}}" id="image_form" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <input type="file" class="custom-file-input" id="image" multiple name="image" onchange="$('#image_form').submit();" ref="file">
                                    <input type="hidden" id="fece5_id" name="id" value="{{$fece5->id}}">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <div class="row no-print">
                <div class="col-lg-3">
                    <button type="button" class="btn btn-brand" onclick="printFece5()">چاپ</button>

                </div>
                @if ($fece5->status == 0)
                    <div class="col-lg-8">
                    
                    @if(($fece5->processed_fece5_file != Null))
                        <a href="{{route('fece5s.conformation',['id'=>$fece5->id,'status'=>'1'])}}" class="btn m-btn--square  btn-success" >تایید ف س ۵</a>
                    @endif    
                        <a href="#" onclick="rejectFece5({{$fece5->id}})" class="btn m-btn--square  btn-danger" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">رد</a>
                    </div>
                @endif

        </div>
    </div>
</div>

{{-- reject fece5 model  --}}
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">توضیحات رد ف س ۵</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="{{route('fece5s.reject_fece5',$fece5->id)}}">
          @csrf
          <div class="form-group">
            <label for="message-text" class="col-form-label">توضیحات</label>
            <textarea class="form-control" id="message-text" name="reject_remark"></textarea>
          </div>
      </div>
      <div class="modal-footer float-right">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">بستن</button>
        <button type="submit" class="btn btn-primary">ثبت</button>
      </form>
      </div>
    </div>
  </div>
</div>

@endsection
@push('custom-css')
<style>
    table.table tr th, table.table tr td {
        border: 1px solid #777;
    }
    table .with_background {
        background: gainsboro !important;

    }
    table .with_background th{
        font-weight: bold;
        white-space: nowrap;
    }
    table tbody tr td {
        border-bottom: 0px !important;
        border-top: 0px !important;
    }
     /* image slide hover styles */

     .update-image-wrapper {
            position: relative;
            /* width: 50%; */
        }

        .image {
            display: block;
            width: 100%;
            height: auto;
        }

        .overlay {
            position: absolute;
            bottom: 0;
            left: 0;
            right: 0;
            background-color: #008cba5e;
            overflow: hidden;
            width: 100%;
            height: 0;
            transition: .5s ease;
        }

        .update-image-wrapper:hover .overlay {
            height: 100%;
        }

        .text {
            color: white;
            font-size: 20px;
            position: absolute;
            top: 50%;
            left: 50%;
            -webkit-transform: translate(-50%, -50%);
            -ms-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
            text-align: center;
        }

        .text a {
            background: #fff;
            padding: 5px 10px;
            margin: 3px;
            font-size: .7em;
        }

        .text a:hover {
            text-decoration: none;
        }
        .w-100px{
            width: 150px;
        }

</style>

@endpush
@push('custom-js')
  <script>

  //print fece5
    function printFece5(){

      window.print()

    }
    window.onafterprint = function(){
        window.location.href="{{route('fece5s.index')}}";
        // window.close();
    }
  </script>
@endpush
