@extends('layouts.master')
@section('title', trans('global.item_details'))
@section('content')
<div class="row m--margin-bottom-20">
        <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
            <label>{{trans('global.name')}}:</label>
            <input type="text" onkeyup="serach()" id="name" class="form-control m-input" placeholder="{{trans('global.name')}}" data-col-index="4">
        </div>
        <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
            <label>{{trans('global.main_category')}}:</label>
            <input type="text" onkeyup="serach()" id="main_category" class="form-control m-input" placeholder="{{trans('global.main_category')}}" data-col-index="0">
        </div>
        <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
            <label>{{trans('global.sub_category')}}:</label>
            <input type="text" onkeyup="serach()" id="sub_category" class="form-control m-input" placeholder="{{trans('global.sub_category')}}" data-col-index="1">
        </div>
      
        <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
            <label>{{trans('global.end_category')}}:</label>
            <input type="text" onkeyup="serach()" id="end_category" class="form-control m-input" placeholder="{{trans('global.end_category')}}" data-col-index="4">
        </div>
</div>
<div class="m-separator m-separator--md m-separator--dashed"></div>
<div class="responsive-table" id="data_table">
    @include('item_details/item_details_list')
</div>    
@endsection
@push('create-button')
<a href="{{ route('item_details.create') }}" class="btn btn-success m-btn m-btn--custom ">
    <span>
    <span>اضافه نمودن</span>
    <i class="la la-plus"></i>
    </span>
</a>
@endpush
@push('custom-js')
<script>
  $.ajaxSetup({
  headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
  });

  $(document).ready(function(){
    
    $(document).on('click','.pagination a',function(event){
      event.preventDefault();
      var page = $(this).attr('href').split('page=')[1];
      get_data(page);

    });
    function get_data(page){
      $.ajax({
          url:'{{url("")}}/get_item_details_ajax?page='+page,
          beforeSend: function() {
        // setting a timeout
          $("#data_table").prepend('<div class="m-blockui "><span>لطفا منتظر باشید </span><span><div class="m-loader  m-loader--brand m-loader--lg"></div></span></div>');
          },
          success:function(data){

              $("#data_table").html(data);
          }
      });
    }   
  });
  function serach(){
      var name = $('#name').val();
      var main_category = $('#main_category').val();
      var sub_category = $('#sub_category').val();
      var end_category = $('#end_category').val();
      $.ajax({
          type:"post",
          url: "{{route('item_details_search')}}",
          data:{
            'name' : name,
            'main_category' : main_category,
            'sub_category' : sub_category,
            'end_category' : end_category
          },
          dataType: "text",
          beforeSend: function() {
        // setting a timeout
          $("#data_table").prepend('<center><div class="m-blockui justify-content-center"><span>لطفا منتظر باشید </span><span><div class="m-loader  m-loader--brand m-loader--lg"></div></span></div></center>');
          },
          success:function(data){
            $("#data_table").html(data);
          }

      });
    }
</script>

@endpush
@push('custom-css')
    <style>
      #data_table .m-blockui {
        color: #5867dd;
        text-align: center !important;
        position: fixed;
        top: 30%;
        right: 50%;
        display: inline-table;
      }
    </style>  
@endpush
