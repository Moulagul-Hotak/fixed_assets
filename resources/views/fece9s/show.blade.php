@extends('layouts.master')
@section('title','نمایش ف س ۹')
@section('content')
  <div class="m-portlet__body">
     <div class="container-fluid" >
      <div >
       <div class="row">
            <div class="col-md-4">
               <b>قرار ۱. شماره:&zwnj; </b> {{$fece9->number}}
            </div>
            <div class="col-md-4">
                <h3>درخواست تحویلخانه</h3>
            </div>
            <div class="col-md-4">
               <b>فورمه ف س ۹</b>
            </div>
       </div>
       <div class="row">
            <div class="col-md-4">
               <b>تاریخ :</b>  {{$fece9->date}}
            </div>
            <div class="col-md-4">

            </div>
            <div class="col-md-4">
               <b>۳. اسم شعبه درخواست کننده</b>.....................
            </div>
       </div>
       <br>
       <div class="row mb-3" >
         <table class="table-fece9">
           <thead>
            <tr>
               <th width="7%">شماره اقلام</th>
               <th width="8%">شماره ذخیره</th>
               <th width="60%">تفصیلات جنس خدمات</th>
               <th>مقدار</th>
               <th>واحد</th>
               <th>معامله شد بحساب</th>
            </tr>
           </thead>
           <tbody>
               <tr class="item-description">
                   <td>1</td>
                   <td>2</td>
                   <td>
                        <p style="text-align: right;">{!!$fece9->description!!}</p>
                        <ul class="list-group list-group-flush text-left">
                            @foreach ($fece9->itemDetails as $index => $value)
                                <li class="list-group-item">{{$value->name_dr}}</li>
                            @endforeach
                        </ul>
                   </td>
                   <td>
                       <p style="text-align: right;">&nbsp;</p>
                        <ul class="list-group list-group-flush item-count">
                            @foreach ($fece9->itemDetails as $index => $value)
                                <li class="list-group-item border border-dark rounded-circle">{{$value->pivot->quantity}}</li>
                            @endforeach
                        </ul>
                   </td>
                   <td>
                       <p style="text-align: right;">&nbsp;</p>
                        <ul class="list-group list-group-flush item-unit">
                            @foreach ($fece9->itemDetails as $index => $value)
                                <li class="list-group-item">{{$value->unit->name_dr}}</li>
                            @endforeach
                        </ul>
                   </td>
                   <td>

                   </td>

               </tr>
               <tr>
                    <td colspan="5">
                        اقلام.  که در آن خط گرفته شده است در تحویلخانه موجود نیست خریداری آن را درخواست گردد
                        <br><br>
                        <div class="row">
                        <div class="col-md-6">
                            --------------------<br>
                            آمر تحویلخانه
                        </div>
                        <div class="col-md-6">
                            ---------------------<br>
                            آمر تحویلخانه
                        </div>
                        </div>
                    </td>
                    <td >
                       نمبر تکت توزیع
                    </td>
                </tr>
            </tbody>
         </table>
       </div>
     </div>
     <div class="row no-print">
         <div class="col-md-4 mb-3 pl-0">
            <a href="{{asset($fece9->processed_fece9_file)}}" data-fancybox data-caption="{{$fece9->description}}">
                <img class="w-100px" src="{{asset('public/images/pdf-2.png')}}"  alt="" />
            </a>

            <a href="#">
                <i class="fa fa-trash "></i>
            </a>
         </div>
     </div>
     <div class="row no-print" >
        <div class="col-lg-3 p-0">
            <button type="button" class="btn btn-brand" onclick="window.print();">چاپ</button>
            {{-- <button type="button" class="btn btn-brand">ارسال</button> --}}
            <a href="{{route('fece9s.index')}}" class="btn btn-secondary">لغو</a>
        </div>
    </div>
    </div>
  </div>
  <style>
    .list-group-item{
      border: none;
    }
    .item-count li {
        padding: 10px 15px;
    }
    .list-group.list-group-flush li {
        margin: 2px;
    }
    .item-description {
        height: 15cm;;
    }
    .col-md-4{

    font-size: 1.6em;
    }
    p , tr , .list-group-item{
        font-size: 1.4em;
    }

    table{
    border-top: 3px groove gray;
    border-right: 3px groove gray;
    border-left: 3px groove gray;
    }


    .w-100px{
        width: 150px;
    }
</style>
@endsection
