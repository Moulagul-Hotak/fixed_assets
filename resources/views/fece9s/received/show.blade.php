@extends('layouts.master')
@section('title','ترتیب ف س ۵')
@section('content')
<div id="show_fece9">
    <legend class="mt-2 text-dark font-weight-bold">مشخصات ف س ۹</legend>
    <div class="row m-row--no-padding m-row--col-separator-xl">
            <div class="col-md-6 meem7_descriptions">
                <div class="m-widget24__item">
                    <h4 class="m-widget24__title">
                        {{trans('global.fece9_number')}}
                    </h4>
                    <span class="m-widget24__stats m--font-brand pull-left">
                            @{{fece9.number}}
                    </span>

                </div>
            </div>
            <div class="col-md-6 meem7_descriptions">
                <div class="m-widget24__item">
                    <h4 class="m-widget24__title">
                        {{trans('global.fece9_date')}}
                    </h4>
                    <span class="m-widget24__stats m--font-brand pull-left">
                            @{{fece9.date}}
                    </span>
                </div>
            </div>
            <div class="col-md-6 meem7_descriptions">
                <div class="m-widget24__item">
                    <h4 class="m-widget24__title">
                        {{trans('global.requesting_department')}}
                    </h4>
                    <span  v-if="fece9.department" class="m-widget24__stats m--font-brand pull-left">
                        @{{fece9.department.name_dr}}
                    </span>

                </div>
            </div>
            <div class="col-md-6 meem7_descriptions">
                <div class="m-widget24__item">
                    <h4 class="m-widget24__title"> 
                            نمایش فایل
                    </h4>
                    <span class="m-widget24__stats m--font-brand pull-left">
                        <a :href="'{{asset('/')}}'+fece9.processed_fece9_file"  data-fancybox><i class="flaticon-attachment"></i></a>
                    </span>
                </div>
            </div>
        </div>
    <form class="m-form m-form--fit m-form--label-align-right" action="{{route('fece5s.store')}}" enctype="multipart/form-data" method="POST">
      @csrf
        <div class="row">
            <div class="col-md-12">
                <div id="accordion" class="mt-2">
                    <legend v-if="fece5s.length" class="mt-2 text-dark font-weight-bold"> ف س ۵ های ترتیب شده</legend>
                    <div v-for="(item, index) in fece5s" class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button type="button" class="btn btn-link" data-toggle="collapse" :data-target="'#ac' + index" aria-expanded="true" aria-controls="collapseOne">
                                        <span class="mr-2">شماره ف س ۵</span><span class="text-dark pr-1">(@{{item.number}})</span>  --  <span class="mr-2">تاریخ ف س ۵:</span><span class="text-dark pr-1">(@{{item.date}})</span> -- <span class="mr-2">توزیع کننده:</span> <span class="text-dark pr-1">
                                            {{-- (@{{item.distributerEmployee.name_dr}}) --}}
                                        </span>
                                </button>
                            </h5>
                        </div>

                        <div :id="'ac' + index" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>شماره</th>
                                                <th>اسم جنس</th>
                                                <th>تعداد درخواست شده</th>
                                                <th>تسلیمی</th>
                                                <th>عملیات</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(item_details, index) in item.item_details">
                                                <td>@{{++index}}</td>
                                                <td>@{{item_details.name_dr}}</td>
                                                <td>@{{item_details.requested_quantity}}</td>
                                                <td>
                                                    <span class="d-block">
                                                        25452541 <i class="fa fa-arrow-right text-success"></i> <i class="fa fa-arrow-left text-success"></i> احمد فرخ
                                                    </span>
                                                    <span class="d-block">
                                                        25452541 <i class="fa fa-arrow-right text-success"></i> <i class="fa fa-arrow-left text-success"></i> احمد شهیر
                                                    </span>
                                                </td>
                                                <td>
                                                    <button class="btn btn-success">
                                                        <i class="fa fa-eye"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <legend class="mt-2 text-dark font-weight-bold"> اجناس درخواست شده</legend>
                <table class="table">
                    <thead>
                        <tr>
                            <th>شماره</th>
                            <th>اسم جنس</th>
                            <th>تعداد درخواست شده </th>
                            <th>تعداد انتخاب شده </th>
                            <th>چگونگی توزیع اجناس</th>
                            <th>عملیات</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-if="item.pivot.status==1 && item.pivot.distributed == 0" v-for="(item, index) in fece9.item_details">
                            <td>@{{++index}}</td>
                            <td>@{{item.name_dr}}</td>
                            <td v-if="item.pivot">@{{item.pivot.quantity}}</td>
                            <td v-if="item.pivot">@{{allotment_list[item.id] ? allotment_list[item.id].length : 0}}</td>
                            <td v-if="item.pivot">
                                <span v-for="(item, index) in allotment_list[item.id]" class="d-block">
                                    <span v-if="item.col1">
                                            @{{extra_specification_keys.col1}}:
                                            <span class="text-success"> @{{item.col1}} |</span>
                                    </span>
                                    <span v-if="item.col2">
                                            @{{extra_specification_keys.col2}}:
                                            <span class="text-success"> @{{item.col2}} |</span>
                                    </span>
                                    <span v-if="item.col3">
                                            @{{extra_specification_keys.col3}}:
                                            <span class="text-success"> @{{item.col3}} |</span>
                                    </span>
                                    <span v-if="item.col4">
                                            @{{extra_specification_keys.col4}}:
                                            <span class="text-success"> @{{item.col4}} |</span>
                                    </span>
                                    <span v-if="item.col5">
                                        @{{extra_specification_keys.col5}}:
                                        <span class="text-success"> @{{item.col5}} |</span>
                                </span>
                                <span v-if="item.col6">
                                        @{{extra_specification_keys.col6}}:
                                        <span class="text-success"> @{{item.col6}} |</span>
                                </span>
                                <span v-if="item.col7">
                                        @{{extra_specification_keys.col7}}:
                                        <span class="text-success"> @{{item.col7}} |</span>
                                </span>
                                <span v-if="item.col8">
                                        @{{extra_specification_keys.col8}}:
                                        <span class="text-success"> @{{item.col8}} |</span>
                                </span>
                                <span v-if="item.col9">
                                        @{{extra_specification_keys.col9}}:
                                        <span class="text-success"> @{{item.col9}} |</span>
                                </span>
                                <span v-if="item.col10">
                                        @{{extra_specification_keys.col10}}:
                                        <span class="text-success"> @{{item.col10}} |</span>
                                </span>
                                <span v-if="item.col11">
                                        @{{extra_specification_keys.col11}}:
                                        <span class="text-success"> @{{item.col11}} |</span>
                                </span>
                                <span v-if="item.col12">
                                        @{{extra_specification_keys.col12}}:
                                        <span class="text-success"> @{{item.col12}} |</span>
                                </span>
                                    <span v-if="item.price">
                                            قیمت:
                                            <span class="text-success"> @{{item.price}} | </span>
                                    </span>
                                    <span>
                                            واحد پولی:
                                            <span v-if="item.currency == 0" class="text-success">افغانی</span>
                                            <span v-else-if="item.currency == 1" class="text-success">دالر</span>
                                    </span>

                                       <i class="fa fa-arrow-right text-success"></i> <i class="fa fa-arrow-left text-success"></i>@{{item.receiver_employee_name}}
                                </span>
                            </td>
                            <td>
                                <span v-if="fece9.fece9_type !=0">
                                    <span v-if="completed_item_details[item.id]" class="text-success text-center m-1">
                                        <i class="fa fa-check"></i>
                                    </span>
                                    <a href="javascript:void(0)"  v-if="completed_item_details[item.id]" class=" text-info m-1" @click.prevent="editItemDetailsSpecificationsFromAllotmentList(index-1, item.id)">
                                        <i class="fa fa-edit"></i>
                                    </a>

                                    <a href="javascript:void(0)"  v-else class="text-primary m-1" @click.prevent="getItemDetailsSpecifications(index-1, item.id)">
                                        <i class="fa fa-list"></i>
                                    </a>
                                </span>
                                <span v-else>
                                    <div class="form-group m-0">


                                        <v-select 
                                            label="name_dr" 
                                            {{-- v-if="item.receiver_employee_id != null" --}}
                                            :options="employees"
                                            item-text="name_dr"
                                            dir="rtl"
                                            v-bind:value="employees.id"
                                            {{-- :reduce="employees=> employees.id" --}}
                                            v-model="item.receiver_employee_name"
                                            @search="onSearch"
                                            {{-- @input="addItemDetailsSpecificationsToAllotmentList(index-1,$event)"                                         --}}
                                         >
                                         
                                         <template slot="no-options">
                                            لطفآ اسم را بنویسید        
                                        </template>
                                        <template slot="option" slot-scope="option">
                                                @{{  option.name_dr }}
                                                ولد:
                                                @{{  option.father_name_dr }}
                                        </template>
                                        </v-select>



                                    </div>
                                    <button type="button" v-if="fece9.item_details.length == index"  @click.prevent="getItemDetailsSpecifications(index-1, item.id)" class="btn btn-success">اضافه نمودن ف س ۵ </button>

                                
                                </span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row " v-if="item_details_specifications.length">
            <div class="col-md-12">
                <legend class="mt-2 text-dark font-weight-bold"> اجناس درخواست شده <span v-if="item_details_specifications.length" :class="[alloted == max_allotments ? 'text-success' : '']">(@{{alloted}} از @{{max_allotments}})</span></legend>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>
                                    <label class="m-checkbox">
                                        <input type="checkbox" @change="selectAllItems($event)" class="checkbox">

                                        <span></span>
                                    </label>
                                    انتخاب همه
                                </th>
                                <th>شماره</th>
                                <th>قیمت </th>
                                <th>واحد پولی </th>
                                <th v-if="extra_specification_keys.col1">
                                    @{{extra_specification_keys.col1}}
                                </th>
                                <th v-if="extra_specification_keys.col2">
                                    @{{extra_specification_keys.col2}}
                                </th>
                                <th v-if="extra_specification_keys.col3">
                                    @{{extra_specification_keys.col3}}
                                </th>
                                <th v-if="extra_specification_keys.col4">
                                    @{{extra_specification_keys.col4}}
                                </th>
                                <th v-if="extra_specification_keys.col5">
                                    @{{extra_specification_keys.col5}}
                                </th>
                                <th v-if="extra_specification_keys.col6">
                                    @{{extra_specification_keys.col6}}
                                </th>
                                <th v-if="extra_specification_keys.col7">
                                    @{{extra_specification_keys.col7}}
                                </th>
                                <th v-if="extra_specification_keys.col8">
                                    @{{extra_specification_keys.col8}}
                                </th>
                                <th v-if="extra_specification_keys.col9">
                                    @{{extra_specification_keys.col9}}
                                </th>
                                <th v-if="extra_specification_keys.col10">
                                    @{{extra_specification_keys.col10}}
                                </th>
                                <th v-if="extra_specification_keys.col11">
                                    @{{extra_specification_keys.col11}}
                                </th>
                                <th v-if="extra_specification_keys.col12">
                                    @{{extra_specification_keys.col12}}
                                </th>
                                <th>عملیات</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-if="edit==true" v-for="(item, index) in item_details_specifications">
                                    <td>
                                        {{-- <div class="form-group">
                                            <input type="checkbox" @change="itemAllotableToUser(index-1, $event)" class="form-control checkbox">
                                        </div> --}}
                                        <div class="m-checkbox-list">
                                            <label class="m-checkbox" v-if="item.receiver_employee_id>0">
                                                <input type="checkbox" :id="index" checked @change="itemAllotableToUser(index-1, item.id , $event)" class="checkbox">

                                                <span></span>
                                            </label>
                                            <label class="m-checkbox" v-else-if="item.receiver_employee_id == null">
                                                <input type="checkbox" :id="index" disabled @change="itemAllotableToUser(index-1, item.id , $event)" class="checkbox">

                                                <span></span>
                                            </label>
                                        </div>
                                    </td>
                                    <td>@{{++index}}</td>
                                    <td>
                                        @{{item.price}}
                                    </td>
                                    <td>
                                        <span v-if="item.currency == 0">افغانی</span>
                                        <span v-else-if="item.currency == 1">دالر</span>
                                    </td>
                                    <td v-if="extra_specification_keys.col1">
                                        @{{item.col1}}
                                    </td>
                                    <td v-if="extra_specification_keys.col2">
                                        @{{item.col2}}
                                    </td>
                                    <td v-if="extra_specification_keys.col3">
                                        @{{item.col3}}
                                    </td>
                                    <td v-if="extra_specification_keys.col4">
                                        @{{item.col4}}
                                    </td>
                                    <td v-if="extra_specification_keys.col5">
                                        @{{item.col5}}
                                    </td>
                                    <td v-if="extra_specification_keys.col6">
                                        @{{item.col6}}
                                    </td>
                                    <td v-if="extra_specification_keys.col7">
                                        @{{item.col7}}
                                    </td>
                                    <td v-if="extra_specification_keys.col8">
                                        @{{item.col8}}
                                    </td>
                                    <td v-if="extra_specification_keys.col9">
                                        @{{item.col9}}
                                    </td>
                                    <td v-if="extra_specification_keys.col10">
                                        @{{item.col10}}
                                    </td>
                                    <td v-if="extra_specification_keys.col11">
                                        @{{item.col11}}
                                    </td>
                                    <td v-if="extra_specification_keys.col12">
                                        @{{item.col12}}
                                    </td>
                                    <td>
                                        <div :class="['form-group m-0', item.receiver_employee_id == 0 ? 'has-danger' : '']">
                                            {{-- <select v-if="item.receiver_employee_id != null" v-model="item.receiver_employee_id" @change="addItemDetailsSpecificationsToAllotmentList(index-1)" class="form-control form-control-sm m-input employee">
                                                <option v-for="item in employees" :value="item.id" >@{{item.name_dr}}</option>
                                            </select> --}}

                                            <v-select 
                                                label="name_dr" 
                                                v-if="item.receiver_employee_id != null"
                                                :options="employees"
                                                item-text="name_dr"
                                                dir="rtl"
                                                v-bind:value="employees.id"
                                                {{-- :reduce="employees=> employees.id" --}}
                                                v-model="item.receiver_employee_name"
                                                @search="onSearch"
                                                @input="addItemDetailsSpecificationsToAllotmentList(index-1,$event)"                                        
                                             >
                                             
                                             <template slot="no-options">
                                                لطفآ اسم را بنویسید        
                                            </template>
                                            <template slot="option" slot-scope="option">
                                                    @{{  option.name_dr }}
                                                    ولد:
                                                    @{{  option.father_name_dr }}
                                            </template>
                                            </v-select>



                                        </div>
                                    </td>
                                </tr>
                            <tr v-if="edit == false && item.status == 0" v-for="(item, index) in item_details_specifications">
                                <td>
                                    {{-- <div class="form-group">
                                        <input type="checkbox" @change="itemAllotableToUser(index-1, $event)" class="form-control checkbox">
                                    </div> --}}
                                    <div class="m-checkbox-list">
                                        <label class="m-checkbox">
                                            <input type="checkbox" :id="index"  @change="itemAllotableToUser(index-1, item.id, $event)" class="checkbox">

                                            <span></span>
                                        </label>
                                    </div>
                                </td>
                                <td>@{{++index}}</td>
                                <td>
                                    @{{item.price}}
                                </td>
                                <td>
                                    <span v-if="item.currency == 0">افغانی</span>
                                    <span v-else-if="item.currency == 1">دالر</span>
                                </td>
                                <td v-if="extra_specification_keys.col1">
                                    @{{item.col1}}
                                </td>
                                <td v-if="extra_specification_keys.col2">
                                    @{{item.col2}}
                                </td>
                                <td v-if="extra_specification_keys.col3">
                                    @{{item.col3}}
                                </td>
                                <td v-if="extra_specification_keys.col4">
                                    @{{item.col4}}
                                </td>
                                <td v-if="extra_specification_keys.col5">
                                    @{{item.col5}}
                                </td>
                                <td v-if="extra_specification_keys.col6">
                                    @{{item.col6}}
                                </td>
                                <td v-if="extra_specification_keys.col7">
                                    @{{item.col7}}
                                </td>
                                <td v-if="extra_specification_keys.col8">
                                    @{{item.col8}}
                                </td>
                                <td v-if="extra_specification_keys.col9">
                                    @{{item.col9}}
                                </td>
                                <td v-if="extra_specification_keys.col10">
                                    @{{item.col10}}
                                </td>
                                <td v-if="extra_specification_keys.col11">
                                    @{{item.col11}}
                                </td>
                                <td v-if="extra_specification_keys.col12">
                                    @{{item.col12}}
                                </td>
                                <td>
                                    <div :class="['form-group m-0', item.receiver_employee_id == 0 ? 'has-danger' : '']">
                                        {{-- <select v-if="item.receiver_employee_id != null" v-model="item.receiver_employee_id" @change="addItemDetailsSpecificationsToAllotmentList(index-1)" class="form-control form-control-sm m-input employee">
                                            <option v-for="item in employees" :value="item.id" >@{{item.name_dr}}</option>
                                        </select> --}}
                                        <v-select
                                                v-if="item.receiver_employee_id != null"
                                                label="name_dr" 
                                                :options="employees"
                                                item-text="name_dr"
                                                dir="rtl"
                                                {{-- :reduce="employees=> employees.id" --}}
                                                v-model="item.receiver_employee_name"
                                                @search="onSearch"
                                                @input="addItemDetailsSpecificationsToAllotmentList(index-1,$event)"                                        
                                             >
                                             <template slot="no-options">
                                                لطفآ اسم را بنویسید        
                                            </template>
                                            <template slot="option" slot-scope="option">
                                                    @{{  option.name_dr }}
                                                    ولد:
                                                    @{{  option.father_name_dr }}
                                            </template>
                                        </v-select>

                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <button type="button" v-if="alloted == max_allotments" @click="finishAddingToAllotedList()" class="btn btn-success">اضافه نمودن</button>
                </div>
                
            </div>
        <div class="row" v-if="finishAdding">
            <div class="col-lg-12">
                <legend class="mt-2 text-dark font-weight-bold"> معلومات ف س ۵ </legend>

                    <div class="m-portlet__body">
                      <div class="form-group row ">
                        <div :class="['form-group col-md-6 m-form__group' , errors.number ? 'has-danger' : '']">

                          <label for="fece5_number">{{trans('global.fece5_number')}}</label>
                            <input type="number" name="number" v-model="fece5_number" value="{{old('number')}}" class="form-control m-input m-input--square" id="number" placeholder="{{trans('global.fece5_number')}}">

                                <div v-if="error" class="form-control-feedback">@{{errors.number}}</div>

                        </div>
                        <div class="['form-group col-md-6 pt-0 m-form__group' , errors.date ? 'has-danger' : '']">
                          <label for="date">{{trans('global.fece5_date')}}</label>
                           <div class="input-group date">
                                <input type="text" name="date" id="date" onfocus='$(".persian_date").persianDatepicker();'   class="form-control persian_date" placeholder="{{trans('global.fece5_date')}}">
                                <div class="input-group-append">
                                  <span class="input-group-text">
                                    <i class="la la-calendar"></i>
                                  </span>
                                </div>
                           </div>
                           <div v-if="error" class="form-control-feedback">@{{errors.date}}</div>
                        </div>
                      </div>
                      <div class="form-group row ">
                        <div class="form-group col-md-6 m-form__group {{$errors->has('distributer_employee_id') ? 'has-danger' : ''}}">
                            <label for="distributer_employee_id">{{trans('global.akhz_as_wajh')}}</label>
                            {{-- <select class="form-control employee" name="distributer_employee_id"  v-model="distributer_employee_id" id="distributer_employee_id">
                                <option>{{trans('global.akhz_as_wajh')}}</option>
                                <option v-for="item in employees" :value="item.id" >@{{item.name_dr}}</option>

                            </select> --}}
                                <input type="hidden" :value="distributer_employee_id"  name="distributer_employee_id">
                                <v-select
                                    label="name_dr" 
                                    :options="motamed"
                                    item-text=""
                                    dir="rtl"
                                    {{-- @search="onSearch" --}}
                                    v-bind:value="motamed.id"
                                    :reduce="motamed=> motamed.id"
                                    v-model="distributer_employee_id"                                                                
                                >
                                    <template slot="no-options">
                                        لطفآ اسم را بنویسید        
                                    </template>
                                    <template slot="option" slot-scope="option">
                                            @{{  option.name_dr }}
                                            ولد:
                                            @{{  option.father_name_dr }}
                                            --
                                            @{{option.current_position_dr}}
                                    </template>
                                </v-select>
                                <div class="form-control-feedback">{{$errors->first('distributer_employee_id')}}</div>

                        </div>

                        <div class="form-group m-form__group col-md-6 pt-0">
                            <label for="department_description">{{trans('global.description')}}</label>
                            <textarea name="description" v-model="fece5_description" class="form-control" id="department_description" placeholder="{{trans('global.description')}}">{{old('description')}}</textarea>
                        </div>

                      </div>
                      <input type="hidden" name="fece5_item_details" :value="JSON.stringify(allotment_list)">
                      <input type="hidden" name="fece9_id" value="{{$fece9_id}}">


                        <div class="form-group row ">
                            <div class="form-group col-md-6 m-form__group {{$errors->has('maktoob_number') ? 'has-danger' : ''}}">
                                <button class="btn btn-success">{{trans('global.submit')}}</button>
                            </div>
                        </div>
                    </div>



            </div>
        </div>
    </form>
</div>



@endsection
@push('custom-js')
    <script>
        Vue.component('v-select', VueSelect.VueSelect)
        item_app = new Vue({
            el: '#show_fece9',
            data: {
                //fece9 details
                fece9: '',

                // fece5s
                fece5s: [],


                // item details specifications for allotment
                item_details_specifications: [ ],
                item_details: [],
                item_details_specifications_edit: { },
                extra_specification_keys:'',

                // allotment

                // selected allotment items based on their item details id
                allotment_list: {},

                //maximum available allotments based on requested items
                max_allotments: 0,

                // currently alloted alloted < = max_allotments
                alloted: 0,
                edit: false,

                // current active item details > specs of which has to be selected
                current_item_details_id: 0,

                // to check whether allotment of specs of an item details is completed
                completed_item_details: {},

                // all employees
                employees: [],

                // show fece5 information form after adding allotment items finishes
                finishAdding: false,

                //Fece5
                fece5_number:'',
                fece5_date:'',
                distributer_employee_id:'',
                fece5_description:'',
                fece5_item_details:'',
                // main form validation
                error: false,
                success: false,
                errors: [],
                motamed:[],

            },
            methods: {
                getFece9Details(id) {
                    axios.get("{{url('get_fece9_details')}}/"+id)
                        .then(res => {
                            this.fece9 = res.data;
                            this.fece5s = res.data.fece5s;
                            // this.max_allotments = this.fece9.item_details[].pivot.quantity;
                            // if(this.fece9.fece9_type ==0){

                            //     this.finishAdding = true;
                            // }

                        })
                        .catch(err => {

                        });
                },
                getAllEmployees() {
                    axios.get("{{url('get_all_employees')}}")
                        .then(res => {
                            this.employees = res.data;
                        })
                        .catch(err => {
                            console.log(err);
                        })

                },
                getItemDetailsSpecifications(index, item_details_id) {
                    this.edit = false;
                    axios.get("{{url('get_item_details_specifications')}}/"+item_details_id+"/"+this.fece9.id)
                        .then(res => {
                            if(typeof res.data.fece9_type !== 'undefined'){
                                this.finishAdding = true;
                                this.allotment_list = this.fece9.item_details;
                            }else{
                                this.item_details_specifications = res.data.items;
                                this.extra_specification_keys = res.data.extra_specifications_keys;
                                this.allotment_list[this.current_item_details_id] = []
                            }
                            $('input.checkbox').prop('checked', false);
                            this.alloted = 0;
                            this.max_allotments = this.fece9.item_details[index].pivot.quantity;
                            this.current_item_details_id = item_details_id;
                            this.motamed.push(res.data.motamed)


                        })
                        .catch(err => {

                        })
                },
                itemAllotableToUser(index, id, event) {
                    if(event.target.checked) {
                        if(this.alloted < this.max_allotments) {
                            ++this.alloted;
                            this.item_details_specifications.filter(item => item.id == id)[0].receiver_employee_id = 0;
                        }
                    }
                    else {
                        if(this.alloted <= this.max_allotments) {
                            this.item_details_specifications.filter(item => item.id == id)[0].receiver_employee_id = null;
                            this.allotment_list[this.current_item_details_id] = this.allotment_list[this.current_item_details_id].filter(item => item.id != id);
                            --this.alloted;
                        }
                    }
                    if(this.alloted == this.max_allotments) {
                        $('input.checkbox:not(input.checkbox:checked)').prop('disabled', true);
                    }
                    else {
                        $('input.checkbox:not(input.checkbox:checked)').prop('disabled', false);
                    }
                },
                addItemDetailsSpecificationsToAllotmentList(index,receiver_employee) {

                    receiver_employee_id = receiver_employee.id;
                    receiver_employee_name = receiver_employee.name_dr;

                    
                    if(!this.allotment_list[this.current_item_details_id]) {
                        this.allotment_list[this.current_item_details_id] = []
                    }
                    if(this.allotment_list[this.current_item_details_id].filter(item => item.id == this.item_details_specifications[index].id).length == 0) {
                        this.item_details_specifications[index]['fece9_id'] = "{{$fece9_id}}";
                        // this.item_details_specifications[index]['sub_category_keys'] = null;
                        this.item_details_specifications[index].receiver_employee_id = receiver_employee_id;
                        this.item_details_specifications[index].receiver_employee_name = receiver_employee_name;
                        this.allotment_list[this.current_item_details_id].push(this.item_details_specifications[index]);
                    }
                    // console.log(this.allotment_list[this.current_item_details_id]);


                },
                finishAddingToAllotedList() {
                    if(this.alloted == this.max_allotments && this.item_details_specifications.filter(item => item.receiver_employee_id == 0).length == 0) {
                        this.item_details_specifications_edit[this.current_item_details_id] = this.item_details_specifications;
                        this.item_details_specifications = [];
                        this.completed_item_details[this.current_item_details_id] = true;
                        this.alloted = 0;

                        // show fece5 form and load datepicker for the date field
                        this.finishAdding = true;
                        // $(".persian_date").persianDatepicker();

                    }
                },
                selectAllItems(event) {
                    max = this.max_allotments;
                    if(event.target.checked) {
                        this.alloted = this.max_allotments;
                        for(i=0;i<max;i++){
                            $('#'+i).attr('checked', 'checked');
                            this.item_details_specifications[i].receiver_employee_id = 0;
                        }
                        $('input.checkbox:not(input.checkbox:checked)').prop('disabled', true);
                    }
                    else {

                        for(j=0;j<max;j++)
                            this.item_details_specifications[j].receiver_employee_id = null;

                        this.alloted=0;
                        $('input.checkbox:not(input.checkbox:checked)').prop('disabled', false);
                        $('table tbody input[type=checkbox]').removeAttr('checked');
                    }
                },
                editItemDetailsSpecificationsFromAllotmentList(index,item_details_id){
                    // this.getItemDetailsSpecifications(index-1, item_details_id);
                    this.item_details_specifications = this.item_details_specifications_edit[item_details_id];
                    this.alloted = 0;
                    len = this.item_details_specifications.length;
                    this.edit = true;
                    for(i=0;i<len;i++){

                            if(this.item_details_specifications[i].receiver_employee_id > 0){

                                this.alloted +=1;
                            }

                    }
                    this.max_allotments = this.alloted;

                },
                onSearch(search, loading) {
                    loading(true);
                    this.search(loading, search, this);
                    },
                search: _.debounce((loading, search, vm) => {
                    
                    axios.post(`{{route("get_employee_axios")}}`,{
                                'name' : search
                                })           
                        .then(res => {
                            // res.json().then(json => (vm.employees = json.items));
                            if(typeof res.data.items =='object'){  
                                vm.employees = res.data.items;
                            // this.employees = res.data.items;                           
                            }
                            loading(false);
                        })
                        .catch(err => {
                            console.log(err);
                        });
                        
                }, 350)




            },
            mounted() {
                this.getFece9Details("{{$fece9_id}}");
                // this.getAllEmployees();
            },
        });

        $(document).ready(function(){
            $(".employee").select2({
            ajax: { 
            url: '{{route("get_employee")}}',
            type:"post",
            dataType: 'json',
            delay: 250,
            data: function (params) {
            return {
                searchTerm: params.term
                // search term
                
            };
            },
            processResults: function (response) {
                return {
                results: response
                };
            },
            cache: true
            }
            });
            
        });


    </script>

@endpush
