<table class="table table-striped- table-bordered table-hover table-checkable " id="m_table_1">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">{{trans('global.name')}}</th>
            <th scope="col">{{trans('global.father_name')}}</th>
            <th scope="col">{{trans('global.designation')}}</th>
            <th scope="col">{{trans('global.department')}}</th>
            <th scope="col">{{trans('global.actions')}}</th>
          </tr>
        </thead>
        <tbody>
            @php
                if(count($external_employee) ==0){
                    echo '<tr class="odd"><th valign="top" colspan="6" class="dataTables_empty text-center">معلومات مورد نظر دریافت نشد</th></tr>';
                }
            @endphp
            @foreach ($external_employee as $index => $item)
                <tr id="{{$item->id}}_tr">
                    <th scope="row">{{$item->id}}</th>
                    <td>{{$item->name_dr}} </td>
                    <td>{{$item->father_name_dr}}</td>
                    <td>{{$item->current_position_dr}}</td>
                    <td>
                      {{$item->external_department_id != 0 ? $item->external_department->name_dr :''}}
                    </td>
                    <td>
                        <a class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" href="{{ route('allotments.get_employee_allotments',[encrypt($item->id),'type'=>'1']) }}"><i class="la la-eye"></i></a>
                        <a class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" href="{{ route('external_employee.edit',encrypt($item->id)) }}"><i class="la la-edit"></i></a>
                        <a href="javascript:void(0)" onclick="deleteRecord('{{route('external_employee.destroy', encrypt($item->id))}}','{{$item->id}}_tr');" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill ">
                          <i class="fa fa-trash"></i>
                      </a>
                    </td>
                </tr>
            @endforeach
        </tbody>
</table>
    
 {!! $external_employee->links()!!}
