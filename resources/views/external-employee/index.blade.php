@extends('layouts.master')
@section('title',trans('global.external_employee'))
@section('content')
{{-- jquery-datatable --}}
<form action="" id="searchForm" >
<div class="row m--margin-bottom-20">
   
    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
      <label>{{trans('global.name')}}:</label>
      <input type="text"   name="name_dr" class="form-control m-input" placeholder="{{trans('global.name')}}" data-col-index="0">
    </div>
    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
      <label>{{trans('global.father_name')}}:</label>
      <input type="text"  id="father_name_dr" name="father_name_dr" class="form-control m-input" placeholder="{{trans('global.father_name')}}" data-col-index="1">
    </div>
  
    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
      <label>{{trans('global.designation')}}:</label>
      <input type="text"  id="current_position_dr" name="current_position_dr" class="form-control m-input" placeholder="{{trans('global.designation')}}" data-col-index="4">
    </div>
    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
      <label>{{trans('global.department')}}:</label>
      <select class="form-control select2 m-input m-input--square" name="external_department_id">
        <option value="">--انتخاب--</option>
        @foreach ($external_departments as $value)
            <option {{old('external_department_id') == $value->id ? 'selected' : ''}} value="{{$value->id}}">نام دری&nbsp;&nbsp;({{$value->name_dr}})&nbsp;&nbsp;&nbsp;&nbsp; نام پشتو&nbsp;&nbsp; ({{$value->name_pa}}) </option>
        @endforeach
    </select>
    </div>
  </div>
  <div class="row">
    <div class="col-md-3 mt-3">
        <button type="submit" id="searchButton" class="btn btn-info">
            <i class="fa fa-search"></i> جستجو
        </button>
    </div>
</div>
@push('create-button')
<a href="{{ route('external_employee.create') }}" class="btn btn-success m-btn m-btn--custom ">
    <span>
    <span>اضافه نمودن</span>
    <i class="la la-plus"></i>
    </span>
</a>
@endpush
</form>
  <div class="m-separator m-separator--md m-separator--dashed"></div>
  
<div id="data_table" class="table-responsive">
  
  @include('external-employee.external_employee_list')
</div>
@endsection
@push('custom-js')
<script>
  $.ajaxSetup({
  headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
  });

  $(document).ready(function(){
    
    $(document).on('click','.pagination a',function(event){
      event.preventDefault();
      var page = $(this).attr('href').split('page=')[1];
      get_data(page);

    });
    function get_data(page){
      $.ajax({
          url:'{{url("")}}/get_external_employee_ajax?page='+page,
          beforeSend: function() {
        // setting a timeout
          $("#data_table").prepend('<div class="m-blockui "><span>لطفا منتظر باشید </span><span><div class="m-loader  m-loader--brand m-loader--lg"></div></span></div>');
          },
          success:function(data){
              $("#data_table").html(data);
          }
      });
    }   
  });

$(document).ready(function() {
    $('#searchForm').on('submit', function(event) {
      event.preventDefault(); // Prevent default form submission behavior

      // Retrieve form data
      var formData = $(this).serialize();

      // Construct the URL with the desired route
      var url = '{{ route("external_employee.get_external_employees_search") }}';
          // Get CSRF token from the meta tag
      var csrfToken = $('meta[name="csrf-token"]').attr('content');

      // Send AJAX request
      $.ajax({
        url: url,
        type: 'POST',
        data: formData,
        success: function(response) {
          // console.log(response)
          $("#data_table").html(response);
        },
        error: function(xhr, status, error) {
          // Handle error
        }
      });
    });
  });
</script>
@endpush
@push('custom-css')
    <style>
      #data_table .m-blockui {
        color: #5867dd;
        text-align: center !important;
        position: fixed;
        top: 30%;
        right: 50%;
        display: inline-table;
      }
    </style>  
@endpush
