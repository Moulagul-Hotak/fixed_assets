@extends('layouts.master')
@section('title','ف س ۲ انتظار به م ۷')
@section('content')

<table id="example" class="table table-striped table-bordered jquery-datatable" style="width:100%">
    <thead>
        <tr>
            <th>#</th>
            <th>شماره</th>
            <th>تاریخ</th>
            <th>ریاست</th>
            <th>توضیحات</th>
            <th>حالت</th>
            <th> اپلود فایل م ۷</th>
            <th>عملیات</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($fece2s as $item)
        @php
        $i=1;
        @endphp
        <tr id='{{$item->id}}_tr'>
            <td>{{$i++}}</td>
            <td>{{$item->number}}</td>
            <td>{{$item->date}}</td>
            <td>{{$item->department->name_dr}}</td> 
            <td>{{str_limit($item->summary,50)}}</td>
            <td>
                @if ($item->status == 0)
                <div class="badge badge-secondary">در حال پروسس</div>
                @elseif($item->status == 1)
                    <div class="badge badge-success">تایید شده</div>
                @else
                    <div class="badge badge-danger" title="{{$item->reject_remark}}">رد شده</div>
                @endif
            </td>
            <td>
                @if ($item->status == 0)
                        @if ($item->meem7 == Null)
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#upload_modal"
                                onclick="$('input[name=id]').val({{$item->id}})"
                                class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air">
                                <i class="fa fa-upload"></i>
                            </a>
                        @else
                            <div class="btn-group" role="group">
                                <a href="{{asset($item->meem7)}}" target="_blank"
                                    class="btn btn-outline-info" style="border: none;">
                                    <i class="fas fa-paperclip fa-lg"></i>
                                </a>
                                <a href="javascript:void(0)" class="btn btn-outline-info" style="border: none;"
                                    onclick="openEditModal({{$item->id}}, 'تصحیح فایل');">
                                    <i class="fas fa-redo-alt"></i>
                                </a>
                            </div>                            
                       @endif
                    @endif
            </td>
            <td>
                <a href="{{route('fece2s.show',$item->id)}}"
                    class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air">
                    <i class="la la-eye"></i>
                </a>
                {{-- <a href="{{route('fece2s.edit', $item->id)}}"
                    class="btn btn-outline-brand m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air">
                    <i class="la la-edit"></i>
                </a> --}}
                {{-- <a href="{{route('fece9_print', $item->id)}}"
                    class="btn btn-outline-brand m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air">
                    <i class="la la-print"></i>
                </a> --}}
                {{-- <a href="javascript:void(0)"
                    onclick="deleteRecord('{{route('fece2s.destroy', $item->id)}}','{{$item->id}}_tr');"
                    class="btn btn-outline-brand m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air">
                    <i class="la la-trash"></i>
                </a> --}}
            </td>
        </tr>
        @endforeach
</table>
<!-- Modal -->
<div class="modal fade" id="upload_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">اپلود نمودن   م۷</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('fece2_meem7_attach_file')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="form-group m-form__group col-lg-12">
                        <div></div>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="image" multiple name="image" ref="file">
                            <label class="custom-file-label" for="customFile">انتخاب فایل</label>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="id" value="">
                <div class="modal-footer">
                    <div class="row">

                        <div class="col-md-6">
                            <div class="btn-group m-btn-group" role="group" aria-label="...">
                                <button type="submit" class="btn btn-brand">ارسال <i
                                        class="la la-paperclip"></i></button>
                                <button type="button" class="btn btn btn-info" data-dismiss="modal">لغو</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- End of the Modal  --}}
@endsection
@push('custom-js')
  <script>
    function openEditModal(itemId, title) {
                    $('input[name=id]').val(itemId);
                    $('#upload_modal .modal-title').text(title);
                    $('#upload_modal_submit_button').text('تصحیح');
                    $('#upload_modal').modal('show');
                }
            </script>
@endpush

