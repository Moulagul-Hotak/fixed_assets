@extends('layouts.master')
@section('title', trans('global.edit_motamed_type'))
@section('content')
<form class="m-form m-form--fit m-form--label-align-right" method="POST" action="{{route('motamed_type.update', encrypt($motamed_type->id))}}">
    @csrf
    @method('PUT')
    <div class="m-portlet__body">
        <div class="form-group m-form__group {{$errors->has('name') ? 'has-danger' : ''}}">
            <label for="department_name">{{trans('global.name')}}</label>
            <input name="name" value="{{ $motamed_type->name }}" type="text" class="form-control m-input m-input--square" id="department_name" aria-describedby="emailHelp" placeholder="{{trans('global.name')}}">
            @if($errors->has('name'))
                <div class="form-control-feedback">{{$errors->first('name')}}</div>
            @endif
        </div>
    </div>
    <div class="m-portlet__foot m-portlet__foot--fit">
        <div class="m-form__actions">
            <button type="submit" class="btn btn-primary">{{trans('global.edit')}}</button>
            <button type="reset" class="btn btn-danger">{{trans('global.cancel')}}</button>
            <a class="btn btn-info" href="{{route('motamed_type.index')}}">{{trans('global.back')}}</a>
        </div>
    </div>
</form>
@endsection
