<?php

use Illuminate\Database\Seeder;
use App\MainCategory;

class MainCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MainCategory::truncate();
        $faker = Faker\Factory::create('fa_IR');
        for($i = 0; $i < 20 ; $i++) {
            MainCategory::create(
                [
                    'name_dr' => $faker->name,
                    'description' => $faker->realText($maxNbChars = 200, $indexSize = 2),
                    'nature' => $faker->numberBetween($min = 0, $max = 1),
                ]
            );
        }
    }
}
