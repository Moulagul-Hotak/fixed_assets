<?php

use Illuminate\Database\Seeder;
use App\EndCategory;

class EndCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EndCategory::truncate();
        $faker = Faker\Factory::create('fa_IR');
        for($i = 0; $i < 20 ; $i++) {
            EndCategory::create(
                [
                    'name_dr' => $faker->name,
                    'description' => $faker->realText($maxNbChars = 200, $indexSize = 2),
                    'sub_category_id' => $faker->numberBetween($min = 1, $max = 20),
                ]
            );
        }
    }
}
