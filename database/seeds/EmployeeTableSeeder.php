<?php

use App\Employee;
use Illuminate\Database\Seeder;

class EmployeeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $employees=[
            ['name_dr'=>'احمد فرخ','department_id'=>23, 'current_position_dr' => 'مدیریت عمومی طرح و ایجاد اپلیکیشن ها'],
            ['name_dr'=>'عبدالواسع','department_id'=>25, 'current_position_dr' => 'مدیریت عمومی طرح و ایجاد اپلیکیشن ها'],
            ['name_dr'=>'احمد شهیر','department_id'=>23, 'current_position_dr' => 'مدیریت عمومی طرح و ایجاد اپلیکیشن ها'],
        ];
        foreach($employees as $item) {
            Employee::create($item);
        }
    }
}

