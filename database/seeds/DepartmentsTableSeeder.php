<?php

use Illuminate\Database\Seeder;
use App\Department;

class DepartmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Department::truncate();
        $departments = [
            [
              "id"=> "23",
              "name_dr"=> "ریاست دفتر ریاست عمومی اداره امور ریاست جمهوری",
              "name_en"=> "Chief of Staff Office at AOP",
              "name_pa"=> "ریاست دفتر ریاست عمومی اداره امور ریاست جمهوری",
              "type"=> "1",
              "parent_id"=> "66",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "25",
              "name_dr"=> "معاونیت انسجام امور دولت",
              "name_en"=> "Coordination of State Affairs",
              "name_pa"=> "معاونیت انسجام امور دولت",
              "type"=> "1",
              "parent_id"=> "66",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "28",
              "name_dr"=> "ریاست دفاع و امنیت",
              "name_en"=> "Defense and Security Directorate",
              "name_pa"=> "ریاست دفاع و امنیت",
              "type"=> "1",
              "parent_id"=> "25",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "33",
              "name_dr"=> "ریاست انسجام امور کابینه",
              "name_en"=> "Directorate of Cabinet Coordination Affairs",
              "name_pa"=> "ریاست انسجام امور کابینه",
              "type"=> "1",
              "parent_id"=> "519",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "39",
              "name_dr"=> "معاونیت روابط و آگاهی عامه",
              "name_en"=> "Deputy Public Relations And Outreach ",
              "name_pa"=> "معاونیت روابط و آگاهی عامه",
              "type"=> "1",
              "parent_id"=> "66",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "41",
              "name_dr"=> "ریاست امور فرهنگی و اجتماعی",
              "name_en"=> "Directoriat of Social and Cultural Affairs",
              "name_pa"=> "ریاست امور فرهنگی و اجتماعی",
              "type"=> "1",
              "parent_id"=> "39",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "53",
              "name_dr"=> "ریاست امور حقوقی، تقنینی و قضایی",
              "name_en"=> "Directorate of Legal, Legislative And Judicial Affairs",
              "name_pa"=> "ریاست امور حقوقی، تقنینی و قضایی",
              "type"=> "1",
              "parent_id"=> "25",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "58",
              "name_dr"=> " ریاست عمومی اداره تدارکات ملی",
              "name_en"=> "National Procurement Authority ",
              "name_pa"=> " ریاست عمومی اداره تدارکات ملی",
              "type"=> "1",
              "parent_id"=> "66",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "65",
              "name_dr"=> "",
              "name_en"=> "",
              "name_pa"=> "",
              "type"=> "1",
              "parent_id"=> "234",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "66",
              "name_dr"=> "ریاست عمومی اداره امور ریاست جمهوری ا.ا",
              "name_en"=> "Administrative Office of the President",
              "name_pa"=> "ریاست عمومی اداره امور ریاست جمهوری ا.ا",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "80",
              "name_dr"=> "ریاست جمهوری اسلامی افغانستان",
              "name_en"=> "Islamic Republic of Afghanistan",
              "name_pa"=> "ریاست جمهوری اسلامی افغانستان",
              "type"=> "1",
              "parent_id"=> "0",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "81",
              "name_dr"=> "دفتر خانم اول کشور",
              "name_en"=> "Office of the First Lady",
              "name_pa"=> "دفتر خانم اول کشور",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "83",
              "name_dr"=> "دفتر حامد کرزی رئیس جمهور پیشین",
              "name_en"=> "Office of the Former President (Hamid Karzai)",
              "name_pa"=> "دفتر حامد کرزی رئیس جمهور پیشین",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "84",
              "name_dr"=> "بورد مشورتی امور عدلی و حقوقی",
              "name_en"=> "Judicial and Legal Affairs Advisory Board",
              "name_pa"=> "بورد مشورتی امور عدلی و حقوقی",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "89",
              "name_dr"=> "ریاست انسجام امور مشاورین و کمیسیون های حقیقت یاب",
              "name_en"=> "Directorate of Advisor and Fact Finder Commission Affairs ",
              "name_pa"=> "ریاست انسجام امور مشاورین و کمیسیون های حقیقت یاب",
              "type"=> "1",
              "parent_id"=> "25",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "92",
              "name_dr"=> "معاونیت اول ریاست جمهوری اسلامی افغانستان",
              "name_en"=> "Office of the First Vice President ",
              "name_pa"=> "معاونیت اول ریاست جمهوری اسلامی افغانستان",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "93",
              "name_dr"=> "معاونیت دوم ریاست جمهوری اسلامی افغانستان",
              "name_en"=> "Office of the Second Vice President of the Islamic Republic of Afghanistan",
              "name_pa"=> "معاونیت دوم ریاست جمهوری اسلامی افغانستان",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "94",
              "name_dr"=> "ریاست دفتر معاونیت دوم ریاست جمهوری",
              "name_en"=> "Administrative Office of the Second Vice President",
              "name_pa"=> "ریاست دفتر معاونیت دوم ریاست جمهوری",
              "type"=> "1",
              "parent_id"=> "93",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "95",
              "name_dr"=> "آمریت تحریرات",
              "name_en"=> "آمریت تحریرات",
              "name_pa"=> "آمریت تحریرات",
              "type"=> "1",
              "parent_id"=> "94",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "96",
              "name_dr"=> "آمریت تحلیل و توحید",
              "name_en"=> "آمریت تحلیل و توحید",
              "name_pa"=> "آمریت تحلیل و توحید",
              "type"=> "1",
              "parent_id"=> "94",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "97",
              "name_dr"=> "آمریت مطبوعات",
              "name_en"=> "آمریت مطبوعات",
              "name_pa"=> "آمریت مطبوعات",
              "type"=> "1",
              "parent_id"=> "94",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "98",
              "name_dr"=> "آمریت تکنالوژی معلوماتی",
              "name_en"=> "آمریت تکنالوژی معلوماتی",
              "name_pa"=> "آمریت تکنالوژی معلوماتی",
              "type"=> "1",
              "parent_id"=> "94",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "99",
              "name_dr"=> "آمریت ارتباط عامه",
              "name_en"=> "آمریت ارتباط عامه",
              "name_pa"=> "آمریت ارتباط عامه",
              "type"=> "1",
              "parent_id"=> "94",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "139",
              "name_dr"=> "مشاوریت ارشد ریاست ج. ا. ا در امور مالی و بانکداری(اجمل احمدی)",
              "name_en"=> "Advisory to the President in Banking and Financial Affairs ",
              "name_pa"=> "مشاوریت ارشد ریاست ج. ا. ا در امور مالی و بانکداری",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "174",
              "name_dr"=> "ریاست امور جامعه مدنی",
              "name_en"=> "Directorate of Civil Society Affairs ",
              "name_pa"=> "ریاست امور جامعه مدنی",
              "type"=> "1",
              "parent_id"=> "39",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "175",
              "name_dr"=> "ریاست امور پارلمانی و ارتباطات مردمی",
              "name_en"=> "Directorate of Complaints, People Communications and Parliamentary Affairs",
              "name_pa"=> "ریاست امور پارلمانی و ارتباطات مردمی",
              "type"=> "1",
              "parent_id"=> "39",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "227",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری در امور هوانوردی و ریاست کمیسیون انکشاف اقتصادی میدان های هوایی",
              "name_en"=> "Office of the Senior Advisor to the President and Afghanistan Airfields Economic Development Commission",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری در امور هوانوردی و ریاست کمیسیون انکشاف اقتصادی میدان های هوایی",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "239",
              "name_dr"=> "واحد تسهیلاتی کارگو هوایی و زمینی برای صادرات و تبادله امتعه تجارتی و اساسی با بازارهای منطقوی",
              "name_en"=> "",
              "name_pa"=> "واحد تسهیلاتی کارگو هوایی و زمینی برای صادرات و تبادله امتعه تجارتی و اساسی با بازارهای منطقوی",
              "type"=> "1",
              "parent_id"=> "227",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "268",
              "name_dr"=> "ریاست انسجام تعقیب اوامر و هدایات",
              "name_en"=> "Commandments and Follow-up Directorate ",
              "name_pa"=> "ریاست انسجام تعقیب اوامر و هدایات",
              "type"=> "1",
              "parent_id"=> "519",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "273",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور زراعت و انکشاف - عبدالحمید هلمندی",
              "name_en"=> "Office of the Senior Adviser to the President for Agriculture and Development Affairs",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور زراعت و انکشاف",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "275",
              "name_dr"=> "وزیر مشاوریت ارشد ریاست جمهوری ا. ا در امورتخنیکی وساختمانی، معادن انرژی وآب (انجنیر محمد یوسف پشتون)",
              "name_en"=> " Advisory to the President in technical, constructional, mines, energy and water Affairs",
              "name_pa"=> "وزیر مشاوریت ارشد ریاست جمهوری ا. ا در امورتخنیکی وساختمانی، معادن انرژی وآب ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "276",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امورجامعه مدنی (عادله بهرام)",
              "name_en"=> "Advisor to the President in Civil Society Affairs",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امورجامعه مدنی ل",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "277",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور حل منازعات (قمرالدین شینواری)",
              "name_en"=> "Advisory to the President in Civil Conflicts Resolution Affairs",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور حل منازعات ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "278",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری اسلامی افغانستان (عباس بصیر)",
              "name_en"=> "Senior Advisor to the President ",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری اسلامی افغانستان ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "279",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری اسلامی افغانستان (علی احمد جلالی)",
              "name_en"=> "Senior Advisor to the President",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری اسلامی افغانستان ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "280",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور امنیتی (جمعه خان همدرد)",
              "name_en"=> "Senior Advisor to the President in Security Affairs",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور امنیتی ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "281",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری اسلامی افغانستان ( فیض الله ذکی)",
              "name_en"=> "Senior Advisor to the President",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری اسلامی افغانستان ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "282",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور اجتماعی (ملالی شینواری)",
              "name_en"=> "Advisory to the President in Social Affairs",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور اجتماعی ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "283",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور روابط عامه و استراتیژیک (شاه زمان میوندی)",
              "name_en"=> "Senior Advisor to the President in Strategic and Public Affairs",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور روابط عامه و استراتیژیک ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "284",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور شفافیت و مبارزه با فساد اداری (سردارمحمد روشن)",
              "name_en"=> "Advisor to the President for Transparency in Development Programmes",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور شفافیت و مبارزه با فساد اداری",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "285",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور زیر بناها (داکتر محمد همایون قیومی)",
              "name_en"=> "Advisor to the President in Infrastructure Affairs",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور زیر بناها ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "286",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور داخلی (خالد فاروقی)",
              "name_en"=> "Advisory to the President in Internal Affairs",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور داخلی ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "287",
              "name_dr"=> "    مشاوریت ارشد ریاست جمهوری ا. ا در امور ثقافت وارزشهای اسلامی (پوهاند نعمت الله شهرانی)",
              "name_en"=> "Senior Advisor to the President in Islamic Affairs",
              "name_pa"=> "    مشاوریت ارشد ریاست جمهوری ا. ا در امور ثقافت وارزشهای اسلامی  ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "288",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور اجتماعی (اسدالله وفا)",
              "name_en"=> "Advisory to the President in Social Affairs",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور اجتماعی ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "289",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور سیاسی (نجیب الله مجددی)",
              "name_en"=> "Senior Advisor to the President in Political Affairs",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور سیاسی ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "290",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور ارتباطات عامه (شیر محمد بهادر)",
              "name_en"=> "Advisory to the President in Public Affairs",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور ارتباطات عامه ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "291",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور مطبوعات (فضل کریم فضل)",
              "name_en"=> "Advisor to the President in Media Affairs",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور مطبوعات ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "292",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور علمی و اجتماعی (لعل پادشاه آزمون)",
              "name_en"=> "Senior Advisor to the President in Social and Academic Affairs",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور علمی و اجتماعی ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "293",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور روابط بین المللی (سید حامد گیلانی)",
              "name_en"=> "Adviser to the President in International Relations Affairs",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور روابط بین المللی ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "294",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور صلح (قطب الدین هلال)",
              "name_en"=> "Senior Advisor to the President in Peace Affairs",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور صلح ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "295",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور امنیت خاص (حبیب الله احمدزی)",
              "name_en"=> "Advisory to the President in special security Affairs",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور امنیت خاص ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "296",
              "name_dr"=> "  مشاوریت ارشد ریاست جمهوری  ا.  ا در  امور  سیستم  ها و تکنالوژی معلوماتی (رفیع الدین ملکزی)",
              "name_en"=> "Adviser to the President in IT and Systems Affairs",
              "name_pa"=> "  مشاوریت ارشد ریاست جمهوری  ا.  ا در  امور  سیستم  ها و تکنالوژی معلوماتی ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "297",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور اجتماعی (داکتر فاروق وردک)",
              "name_en"=> "Advisory to the President in Social Affairs",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور اجتماعی ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "298",
              "name_dr"=> "   مشاوریت ارشد و نمایندۀ خاص ریاست جمهوری  ا.  ا در امور انسجام ملی (محمد الماس ذاهد)",
              "name_en"=> "Senior Advisor to the President in National Solidarity Affairs",
              "name_pa"=> "   مشاوریت ارشد و نمایندۀ خاص ریاست جمهوری  ا.  ا در امور انسجام ملی ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "299",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور اصلاحات اداری و خدمات ملکی (داکتر احمد مشاهد)",
              "name_en"=> "Advisory to the President in in Reforms and Public Services",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور اصلاحات اداری و خدمات ملکی ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "300",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور سیاسی (داود گلزار)",
              "name_en"=> "Advisory to the President in Political Affairs",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور سیاسی ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "301",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور انسجام امور جوامع محلی (احمد یوسف نورستانی)",
              "name_en"=> "Senior Advisor to the President in Local Communities Affairs",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور انسجام امور جوامع محلی ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "302",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور روابط مردمی  (محمد ایوب رفیقی)",
              "name_en"=> "Advisory to the President in Public Affairs",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور روابط مردمی  ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "303",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور ملل متحد (فرخنده زهرا نادری)",
              "name_en"=> "Advisory to the President in United Nations Affairs",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور ملل متحد ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "305",
              "name_dr"=> "    مشاوریت ارشـد ریـاست جمهوری ا. ا در امـور اقـوام (فضل کریم ایماق)",
              "name_en"=> "Advisory to the President in Tribal Affairs",
              "name_pa"=> "    مشاوریت ارشـد ریـاست جمهوری ا. ا در امـور اقـوام",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "306",
              "name_dr"=> "وزیر مشاور ریاست جمهوری ا. ا در امور سرحدات و قبایل (محمد گلاب منگل)",
              "name_en"=> "Minister Advisory to the Presidency in Frontier and Tribes Affairs",
              "name_pa"=> "وزیر مشاور ریاست جمهوری ا. ا در امور سرحدات و قبایل ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "307",
              "name_dr"=> "وزیر مشاوریت ریاست جمهوری ا. ا در امور فرهنگی (اسدالله غضنفر)",
              "name_en"=> "Advisory to the President in Cultural Affairs",
              "name_pa"=> "وزیر مشاوریت ریاست جمهوری ا. ا در امور فرهنگی ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "308",
              "name_dr"=> "وزیر مشاوریت ریاست جمهوری ا. ا در امور قومی و اجتماعی (محمد علم راسخ)",
              "name_en"=> "Minister Advisory to the President in Nations and Social Affairs",
              "name_pa"=> "وزیر مشاوریت ریاست جمهوری ا. ا در امور قومی و اجتماعی\n          ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "309",
              "name_dr"=> "    وزیر مشاوریت ریاست جمهوری ا. ا در امور دینی (ایت الله محمد هاشم صالحی)",
              "name_en"=> "Minister Advisory to the President in Religious Affairs",
              "name_pa"=> "    وزیر مشاوریت ریاست جمهوری ا. ا در امور دینی\n          ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "310",
              "name_dr"=> "وزیر مشاوریت ریاست جمهوری ا. ا در امور اجتماعی (سید محمود فارانی)",
              "name_en"=> "Advisory to the President in Social Affairs",
              "name_pa"=> "وزیر مشاوریت ریاست جمهوری ا. ا در امور اجتماعی ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "311",
              "name_dr"=> "وزیر مشاوریت ریاست جمهوری ا. ا در امور قومی (عبدالکریم براهوی)",
              "name_en"=> "Minister Advisory to the Presidency in Nations Affairs",
              "name_pa"=> "وزیر مشاوریت ریاست جمهوری ا. ا در امور قومی\n          ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "312",
              "name_dr"=> "وزیر مشاوریت ریاست جمهوری ا. ا در امور صحت (فیض الله کاکر)",
              "name_en"=> "Minister Advisory to the Presidency in Health Affairs",
              "name_pa"=> "وزیر مشاوریت ریاست جمهوری ا. ا در امور صحت ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "313",
              "name_dr"=> "وزیر مشاوریت ریاست جمهوری ا. ا در امور قومی (وحیدالله سباوون)",
              "name_en"=> "Minister Advisory to the Presidency in Nations Affairs",
              "name_pa"=> "وزیر مشاوریت ریاست جمهوری ا. ا در امور قومی\n          ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "314",
              "name_dr"=> "وزیر مشاوریت و نمایندۀ خاص و فوق العادۀ ریاست جمهوری ا. ا (عبدالقیوم کوچی)",
              "name_en"=> "Advisor and Special Envoy to the President ",
              "name_pa"=> "وزیر مشاوریت و نمایندۀ خاص و فوق العادۀ ریاست جمهوری ا. ا ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "315",
              "name_dr"=> "وزیر مشاوریت ریاست جمهوری ا. ا در امور صحی (دوکتور سهیلا صدیق)",
              "name_en"=> "Minister Advisory to the Presidency in Health Affairs",
              "name_pa"=> "وزیر مشاوریت ریاست جمهوری ا. ا در امور صحی\n          ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "316",
              "name_dr"=> "وزیر مشاوریت ریاست جمهوری اسلامی افغانستان (سید شاه ناصر نادری)",
              "name_en"=> "Minister Advisory to the Presidency",
              "name_pa"=> "وزیر مشاوریت ریاست جمهوری اسلامی افغانستان\n          ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "317",
              "name_dr"=> "وزیر مشاوریت ریاست جمهوری ا. ا در امور تحصیلات عالی (حمیدالله فاروقی)",
              "name_en"=> "Advisory to the President in Higher Education Affairs",
              "name_pa"=> "وزیر مشاوریت ریاست جمهوری ا. ا در امور تحصیلات عالی ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "318",
              "name_dr"=> "وزیر مشاوریت ریاست جمهوری اسلامی افغانستان(معصوم ستانکزی)",
              "name_en"=> "Minister Advisory to the Presidency",
              "name_pa"=> "وزیر مشاوریت ریاست جمهوری اسلامی افغانستان\n          ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "319",
              "name_dr"=> "مشاوریت ریاست جمهوری ا. ا در امور دینی (مولوی محمد جوره)",
              "name_en"=> "Adviser to the President in Religious Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا. ا در امور دینی ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "320",
              "name_dr"=> "مشاوریت ریاست جمهوری ا. ا در امور دینی (محمد عثمان سالکزاده)",
              "name_en"=> "Adviser to the President in Religious Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا. ا در امور دینی ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "321",
              "name_dr"=> "مشاوریت ریاست جمهوری ا. ا در امور قومی و دینی (مولوی محی الدین بلوچ)",
              "name_en"=> "Advisory to the President in Religion and Tribal Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا. ا در امور قومی و دینی ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "322",
              "name_dr"=> "مشاوریت ریاست جمهوری ا. ا در امور رسیده گی به حقوق محجوزین و محبوسین (خلیل الله زمر)",
              "name_en"=> "Advisor to the President in Prisoners Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا. ا در امور رسیده گی به حقوق محجوزین و محبوسین ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "323",
              "name_dr"=> "مشاوریت ریاست جمهوری ا. ا در امور صحی (داکتر تیمور علم)",
              "name_en"=> "Advisory to the President in Health Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا. ا در امور صحی\n          ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "324",
              "name_dr"=> "نماینده ریاست جمهوری ا. ا در امور نظارت از پروژه های زیر بنائی (طارق فورملی)",
              "name_en"=> "Presidency Representative in Monitoring Affairs",
              "name_pa"=> "نماینده ریاست جمهوری ا. ا در امور نظارت از پروژه های زیر بنائی ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "325",
              "name_dr"=> "نماینده خاص ریاست جمهوری اسلامی افغانستان (محمد اکرم خپلواک)",
              "name_en"=> "Special Envoy to the President ",
              "name_pa"=> "نماینده خاص ریاست جمهوری اسلامی افغانستان ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "326",
              "name_dr"=> "نمایندۀ خاص ریاست جمهوری در کشور های مشترک المنافع (محمد شاکر کارگر)",
              "name_en"=> "President special representative for mutually intrested countries",
              "name_pa"=> "نمایندۀ خاص ریاست جمهوری در کشور های مشترک المنافع ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "327",
              "name_dr"=> "مشاوریت ریاست جمهوری ا. ا در امور حقوقی (عبدالستار سعادت)",
              "name_en"=> "Advisory to the President in Civil Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا. ا در امور حقوقی ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "328",
              "name_dr"=> "مشاوریت ریاست جمهوری ا. ا در امور سیاسی ومنطقوی (عبدالرحمن هوتکی)",
              "name_en"=> "Advisory to the President in Political and Regional Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا. ا در امور سیاسی ومنطقوی ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "329",
              "name_dr"=> "مشاوریت ریاست جمهوری ا. ا در امور بین المللی (ریدا عظیمی)",
              "name_en"=> "Advisory to the President in International Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا. ا در امور بین المللی",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "330",
              "name_dr"=> "مشاوریت ریاست جمهوری ا. ا در امور اجتماعی (پیغمبر قل دوغن)",
              "name_en"=> "Advisory to the President in Social Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا. ا در امور اجتماعی ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "331",
              "name_dr"=> "مشاوریت ریاست جمهوری ا. ا در امور حقوق زنان (گلالی احکزی)",
              "name_en"=> "Advisory to the President in Women Rights Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا. ا در امور حقوق زنان ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "332",
              "name_dr"=> "مشاوریت ریاست جمهوری ا. ا در امور اصلاح اداره (عزیز الله آریافر)",
              "name_en"=> "Advisory to the President in Administrative Reforms Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا. ا در امور اصلاح اداره ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "333",
              "name_dr"=> "  مشاوریت ریاست جمهوری ا. ا در امور اجتماعی و فرهنگی (محمد عزیز بختیاری)",
              "name_en"=> "Advisor to the President in Social and Cultural Affairs",
              "name_pa"=> "  مشاوریت ریاست جمهوری ا. ا در امور اجتماعی و فرهنگی ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "334",
              "name_dr"=> "مشاوریت ریاست جمهوری ا. ا در امور معادن (پوهاند محمد حسین گرزیوانی)",
              "name_en"=> "Advisory to the President in Mines Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا. ا در امور معادن ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "335",
              "name_dr"=> "مشاوریت ریاست جمهوری ا. ا در امور تقویت دموکراسی (داکتر لیلا احراری)",
              "name_en"=> "Advisory to the President in Strengthening Democracy Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا. ا در امور تقویت دموکراسی ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "336",
              "name_dr"=> "مشاوریت ریاست جمهوری ا. ا در امور عدلی و قضائی (سلیمان حامد)",
              "name_en"=> "Advisory to the President in Judicial Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا. ا در امور عدلی و قضائی",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "337",
              "name_dr"=> "مشاوریت ریاست جمهوری ا. ا در امور مبارزه با فساد اداری (نادر محسنی)",
              "name_en"=> "Advisory to the President in Anti Corruption Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا. ا در امور مبارزه با فساد اداری",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "338",
              "name_dr"=> "مشاوریت ریاست جمهوری ا. ا در امور حقوق بشر (شریفه زرمتی وردک)",
              "name_en"=> "Advisor to the President in Human Rights Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا. ا در امور حقوق بشر ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "339",
              "name_dr"=> "مشاوریت ریاست جمهوری ا. ا در امور حکومتداری محلی (سریر احمد برمک)",
              "name_en"=> "Advisory to the President in Local Governance Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا. ا در امور حکومتداری محلی ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "340",
              "name_dr"=> "مشاوریت ریاست جمهوری ا. ا در امور سکتور خصوصی (حاجی علی اکبر ژوندی)",
              "name_en"=> "Advisor to the President in Private Sector Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا. ا در امور سکتور خصوصی ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "341",
              "name_dr"=> "مشاوریت ریاست جمهوری ا. ا در امور دینی (سید محمد علی جاوید)",
              "name_en"=> "Advisory to the President in Religious Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا. ا در امور دینی\n          ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "342",
              "name_dr"=> "مشاوریت ریاست جمهوری ا. ا در امور قومی (میر اعظم گجروال)",
              "name_en"=> "Advisory to the President in Ethnic Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا. ا در امور قومی\n          ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "343",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور قومی (محمد طاهر صافی)",
              "name_en"=> "Advisory to the Presidency in Ethnic Affairs ",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور قومی",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "344",
              "name_dr"=> "مشاوریت ریاست جمهوری ا. ا در امور قومی (الحاج محمد عمر)",
              "name_en"=> "Advisory to the President in Ethnic Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا. ا در امور قومی\n          ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "345",
              "name_dr"=> "مشاوریت ریاست جمهوری ا. ا در امور قومی (رحمت الله راشدی)",
              "name_en"=> "Advisory to the President in Ethnic Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا. ا در امور قومی\n          ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "346",
              "name_dr"=> "مشاوریت ریاست جمهوری ا. ا در امور اجتماعی (حفیظ الله محسنی)",
              "name_en"=> "Advisory to the President in Social Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا. ا در امور اجتماعی\n          ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "347",
              "name_dr"=> "مشاوریت ریاست جمهوری ا. ا در امور قومی (محمد عارف نورزی کوچی)",
              "name_en"=> "Advisory to the President in Ethnic Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا. ا در امور قومی\n          ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "348",
              "name_dr"=> "مشاوریت ریاست جمهوری ا. ا در امور قومی (محمد یعقوب شمس)",
              "name_en"=> "Advisory to the President in Ethnic Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا. ا در امور قومی\n          ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "349",
              "name_dr"=> "مشاوریت ریاست جمهوری ا. ا در امور قومی (عبدالهادی واحدی)",
              "name_en"=> "Advisor to the President in Tribal Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا. ا در امور قومی ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "350",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور قومی (الحاج محمد موسی هوتک)",
              "name_en"=> "Senior Advisory to the President in Ethnic Affairs",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور قومی",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "351",
              "name_dr"=> "وزیر مشاوریت ریاست جمهوری ا. ا در امور دینی (مولوی عنایت الله بلیغ)",
              "name_en"=> " Minister Advisory to the President in Religious Affairs",
              "name_pa"=> "وزیر مشاوریت ریاست جمهوری ا. ا در امور دینی ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "352",
              "name_dr"=> "مشاوریت ریاست جمهوری ا. ا در امور دینی (مولوی عبدالحق مظهری)",
              "name_en"=> "Advisory to the President in Religious Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا. ا در امور دینی\n          ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "353",
              "name_dr"=> "مشاوریت ریاست جمهوری ا. ا در امور اجتماعی (همایون شینواری)",
              "name_en"=> "Advisory to the President in Social Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا. ا در امور اجتماعی",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "354",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور اجتماعی (دلبرجان آرمان)",
              "name_en"=> "Senior Advisory to the President in Social Affairs",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور اجتماعی ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "355",
              "name_dr"=> "مشاوریت ریاست جمهوری ا. ا در امور کوچی ها (عبدالقادر کوچی)",
              "name_en"=> "Advisory to the President in Kochies Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا. ا در امور کوچی ها\n          ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "356",
              "name_dr"=> "مشاوریت ریاست جمهوری ا. ا در امور اداری و تشریفاتی (احمد وفی امین)",
              "name_en"=> "Advisory to the President in Administrative and Protocol Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا. ا در امور اداری و تشریفاتی ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "357",
              "name_dr"=> "مشاوریت ریاست جمهوری ا. ا در امور اقوام وقبائیل (صالح محمد صفاری)",
              "name_en"=> "Advisory to the President in Nations and Tribes Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا. ا در امور اقوام وقبائیل\n          ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "358",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور مطبوعات (محمد نبی مصداق)",
              "name_en"=> "Senior Advisory to the President in Media Affairs",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور مطبوعات ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "359",
              "name_dr"=> "مشاوریت ریاست جمهوری ا. ا در امور اقوام (محمد شفیق اسدالله سعید) ",
              "name_en"=> " Advisor to the President in Tribal Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا. ا در امور اقوام  ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "360",
              "name_dr"=> "مشاوریت ریاست جمهوری ا. ا در امور حکومتداری محلی (احمدالله علی زی)",
              "name_en"=> "Advisory to the President in Local Governance Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا. ا در امور حکومتداری محلی",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "361",
              "name_dr"=> "مشاوریت ریاست جمهوری ا. ا در امور اجتماعی (خیال محمد حسینی)",
              "name_en"=> "Advisory to the President in Social Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا. ا در امور اجتماعی ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "362",
              "name_dr"=> "مشاوریت ریاست جمهوری ا. ا در امور سرحدات (نجیب الله ناصر)",
              "name_en"=> "Advisory to the President in Borders Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا. ا در امور سرحدات ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "363",
              "name_dr"=> "مشاوریت ریاست جمهوری ا. ا در امور اجتماعی (همایون جریر)",
              "name_en"=> "Advisory to the President in Social Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا. ا در امور اجتماعی ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "364",
              "name_dr"=> "مشاوریت ریاست جمهوری ا. ا در امور جوانان (محمد شمیم کتوازی)",
              "name_en"=> "Advisory to the President in Youth Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا. ا در امور جوانان\n          ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "366",
              "name_dr"=> "مشاوریت ریاست جمهوری ا. ا در امور علمی (عوض علی سعادت)",
              "name_en"=> "Advisor to the President in Academic Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا. ا در امور علمی ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "367",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور حل منازعات (اسدالله فلاح)",
              "name_en"=> "Advisory to the President in Conflicts Resolution Affairs",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور حل منازعات",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "368",
              "name_dr"=> "مشاوریت ریاست جمهوری ا. ا در امورکوچی ها (حاجی علم کوچی)",
              "name_en"=> "Advisory to the President in Kochies Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا. ا در امورکوچی ها ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "369",
              "name_dr"=> "مشاوریت ریاست جمهوری ا. ا در امور اجتماعی (داکتر عیدمحمد احمدی)",
              "name_en"=> " Advisory to the President in social affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا. ا در امور اجتماعی ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "370",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور تجارتی (حاجی سیروس علاف)",
              "name_en"=> "Advisory to the President in Commerce Affairs",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور تجارتی\n          ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "371",
              "name_dr"=> "مشاوریت ریاست جمهوری ا. ا در امور قومی (حاجی محمد قسیم)",
              "name_en"=> "Advisory to the President in Ethnic Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا. ا در امور قومی\n          ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "372",
              "name_dr"=> "مشاوریت ریاست جمهوری ا. ا در امور قومی درسطح محلات (بسم الله روحانی)",
              "name_en"=> "Advisor to the President in Tribal Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا. ا در امور قومی درسطح محلات ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "373",
              "name_dr"=> "مشاوریت ریاست جمهوری ا. ا در امور قومی (محمد یونس هوتک)",
              "name_en"=> "Advisor to the President in Tribal Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا. ا در امور قومی ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "374",
              "name_dr"=> "مشاوریت ریاست جمهوری ا. ا در امور اجتماعی و جوانان (نسیم شریفی)",
              "name_en"=> "Advisory to the President in Social and Youth Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا. ا در امور اجتماعی و جوانان ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "375",
              "name_dr"=> "مشاوریت ریاست جمهوری ا. ا در امور اجتماعی (عبداللطیف ابراهیمی)",
              "name_en"=> "Advisory to the President in Social Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا. ا در امور اجتماعی ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "376",
              "name_dr"=> "مشاوریت ریاست جمهوری ا. ا در امور پشتیبانی از نیرو های امنیتی و دفاعی (ابوالحسن یاسر)",
              "name_en"=> "Advisor to the President in Security Forces Support Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا. ا در امور پشتیبانی از نیرو های امنیتی و دفاعی ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "377",
              "name_dr"=> "مشاوریت ریاست جمهوری ا. ا در امور معارف، تحصیلات عالی و تکنالوژی معلوماتی (عبدالقادر ودان)",
              "name_en"=> "Advisory to the President in Education, Higher Education and IT Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا. ا در امور معارف، تحصیلات عالی و تکنالوژی معلوماتی ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "378",
              "name_dr"=> "مشاوریت ریاست جمهوری ا. ا در امور قومی (عبدالحلیم)",
              "name_en"=> "Advisory to the President in Religions Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا. ا در امور قومی ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "379",
              "name_dr"=> "مشاوریت ریاست جمهوری ا. ا در امور توسعۀ اقتصادی و امور زنان (درخشان عصمتی)",
              "name_en"=> "Adviser to the President in Women",
              "name_pa"=> "مشاوریت ریاست جمهوری ا. ا در امور توسعۀ اقتصادی و امور زنان ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "380",
              "name_dr"=> "مشاوریت ریاست جمهوری ا. ا در امور قومی (محمد ابراهیم مشفق)",
              "name_en"=> "Advisor to the President in Tribal Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا. ا در امور قومی ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "381",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور مردمی (امان الله)",
              "name_en"=> "Advisory to the President in Public Affairs",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور مردمی ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "382",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور صحت عامه (رضوان الله احمدزی)",
              "name_en"=> "Adviser to the President in Public Health Affairs",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور صحت عامه ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "383",
              "name_dr"=> "مشاوریت ریاست جمهوری ا. ا در امور قومی (محمد اسحق بهمن)",
              "name_en"=> "Advisor to the President in Public Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا. ا در امور قومی ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "385",
              "name_dr"=> "مشاوریت ریاست جمهوری ا. ا در امور قومی (محمد طاهر صبری)",
              "name_en"=> "Senior Advisory to the President in Ethnic Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا. ا در امور قومی\n          ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "386",
              "name_dr"=> "مشاوریت ریاست جمهوری ا. ا در امور قومی (توردی آخند)",
              "name_en"=> "Advisor to the President in Tribal Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا. ا در امور قومی ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "387",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور حل منازعات مردمی (مختار رشیدی)",
              "name_en"=> "Advisor to the President in Public Conflicts Resolution Affairs",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور حل منازعات مردمی",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "388",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور مهاجرین و انکشاف اجتماعی (خیبر فراهی)",
              "name_en"=> "Senior Advisor to the President in Social Development and Migration Affairs",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور مهاجرین و انکشاف اجتماعی",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "391",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور روابط عامه و استراتیژیک (نادر نادری)",
              "name_en"=> "Senior Advisor to the President in Strategic and Public Affairs",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا. ا در امور روابط عامه و استراتیژیک",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "392",
              "name_dr"=> "مشاوریت ریاست جمهوری ا.ا در امورفرهنگی و اجتماعی (محمد محق)",
              "name_en"=> "Advisor to the President in Cultural and Social Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا.ا در امورفرهنگی و اجتماعی",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "393",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور نظامی (ستر جنرال افضل لودین)",
              "name_en"=> "Senior Advisor to the President in Military Affairs",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور نظامی",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "394",
              "name_dr"=> "ریاست عمومی دفتر مقام عالی ریاست جمهوری",
              "name_en"=> "General Direcotorait of The Presidential Office",
              "name_pa"=> "ریاست عمومی دفتر مقام عالی ریاست جمهوری",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "395",
              "name_dr"=> "ریاست عمومی اداره امور ریاست جمهوری اسلامی افغانستان",
              "name_en"=> "Administrative Office of the President",
              "name_pa"=> "ریاست عمومی اداره امور ریاست جمهوری اسلامی افغانستان",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "396",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور منابع طبیعی (انجینیر شیرمحمدجامی زاده)",
              "name_en"=> "Senior Advisory of the president at Natural resources",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور منابع طبیعی",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "397",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور هماهنگی و تطبیق پالیسی های شوراهای عالی ریاست جمهوری (شهرزاد اکبر)",
              "name_en"=> "Office of the Senior Advisor to the President at supreme councils coordination",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور هماهنگی و تطبیق پالیسی های شوراهای عالی ریاست جمهوری",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "398",
              "name_dr"=> "وزیر مشاوریت ریاست جمهوری ا.ا در امور امنیتی (تاج محمد جاهد)",
              "name_en"=> "Advisory to the President in Security Affairs",
              "name_pa"=> "وزیر مشاوریت ریاست جمهوری ا.ا در امور امنیتی",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "399",
              "name_dr"=> "مشاوریت ریاست جمهوری ا.ا در امور مردمی در حوزه جنوب غرب (صادق قادری)",
              "name_en"=> "Advisory to the President in Public Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا.ا در امور مردمی در حوزه جنوب غرب",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "400",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور انسجام حمایت مردمی و معاون دفتر شورای امنیت ملی جمهوری ا.ا (دین محمد جرات)",
              "name_en"=> "",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور انسجام حمایت مردمی و معاون دفتر شورای امنیت ملی جمهوری ا.ا",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "401",
              "name_dr"=> "نماینده خاص ریاست جمهوری ا.ا در امور صلح در حوزه جنوب غرب (محمدمسعودآخندزاده)",
              "name_en"=> " Special Envoy to the President for Peace Affairs",
              "name_pa"=> "نماینده خاص ریاست جمهوری ا.ا در امور صلح در حوزه جنوب غرب",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "402",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور مردمی و اجتماعی (داکترمحمدرسول طالب)",
              "name_en"=> "Senior Advisory to the President in Folks and Social Affairs",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور مردمی و اجتماعی",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "403",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور اجتماعی (محمدانور جگدلک)",
              "name_en"=> "Senior Advisor to the president in Social Affairs",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور اجتماعی",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "404",
              "name_dr"=> "معاونیت نظارت و تدارکات الکترونیکی",
              "name_en"=> "Deputy of Monitoring and E-Procurement",
              "name_pa"=> "معاونیت نظارت و تدارکات الکترونیکی",
              "type"=> "1",
              "parent_id"=> "58",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "405",
              "name_dr"=> "معاونیت مسلکی",
              "name_en"=> "Deputy of Policy",
              "name_pa"=> "معاونیت مسلکی",
              "type"=> "1",
              "parent_id"=> "58",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "406",
              "name_dr"=> "معاونیت عملیاتی و منابع",
              "name_en"=> "Deputy of Operation and Recource",
              "name_pa"=> "معاونیت عملیاتی و منابع",
              "type"=> "1",
              "parent_id"=> "58",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "407",
              "name_dr"=> "ریاست اسناد و ارتباط",
              "name_en"=> "Correspondance Directorate",
              "name_pa"=> "ریاست اسناد و ارتباط",
              "type"=> "1",
              "parent_id"=> "23",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "408",
              "name_dr"=> "اداره عملیاتی و حمایوی انکشاف ملی ریاست جمهوری ا.ا",
              "name_en"=> "Operational and Supportive Unit of Presidency",
              "name_pa"=> "اداره عملیاتی و حمایوی انکشاف ملی ریاست جمهوری ا.ا",
              "type"=> "1",
              "parent_id"=> "66",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "409",
              "name_dr"=> "ریاست منابع بشری",
              "name_en"=> "Directorate of Human Resources",
              "name_pa"=> "ریاست منابع بشری",
              "type"=> "1",
              "parent_id"=> "469",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "410",
              "name_dr"=> "ریاست مالی",
              "name_en"=> "Directorate of Finance",
              "name_pa"=> "ریاست مالی",
              "type"=> "1",
              "parent_id"=> "469",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "411",
              "name_dr"=> "ریاست تکنالوژی معلوماتی",
              "name_en"=> "Information Technology Directorate",
              "name_pa"=> "ریاست تکنالوژی معلوماتی",
              "type"=> "1",
              "parent_id"=> "468",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "412",
              "name_dr"=> "ریاست خدمات",
              "name_en"=> "Directorate of Services",
              "name_pa"=> "ریاست خدمات",
              "type"=> "1",
              "parent_id"=> "468",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "413",
              "name_dr"=> "ریاست تعمیرات, حفظ ومراقبت",
              "name_en"=> "Directoraite of Maintaince",
              "name_pa"=> "ریاست تعمیرات, حفظ ومراقبت",
              "type"=> "1",
              "parent_id"=> "469",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "414",
              "name_dr"=> "ریاست تدارکات",
              "name_en"=> "Directorate Of Procurement",
              "name_pa"=> "ریاست تدارکات",
              "type"=> "1",
              "parent_id"=> "468",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "415",
              "name_dr"=> "ریاست تخنیک و ترانسپورت",
              "name_en"=> "Directorate of Techniques and Transport",
              "name_pa"=> "ریاست تخنیک و ترانسپورت",
              "type"=> "1",
              "parent_id"=> "468",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "416",
              "name_dr"=> "ریاست اقتصاد ملی",
              "name_en"=> "Directorate of National Economy",
              "name_pa"=> "ریاست اقتصاد ملی",
              "type"=> "1",
              "parent_id"=> "25",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "417",
              "name_dr"=> "ریاست روابط بین الملل",
              "name_en"=> "International Communication Directorate",
              "name_pa"=> "ریاست روابط بین الملل",
              "type"=> "1",
              "parent_id"=> "25",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "418",
              "name_dr"=> "ریاست مطبوعات و برنامه ها",
              "name_en"=> "Directorate of Media and Programms",
              "name_pa"=> "ریاست مطبوعات و برنامه ها",
              "type"=> "1",
              "parent_id"=> "39",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "419",
              "name_dr"=> "ریاست عرایض وشکایات",
              "name_en"=> "Directorate of Applications and Complaints",
              "name_pa"=> "ریاست عرایض وشکایات",
              "type"=> "1",
              "parent_id"=> "23",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "420",
              "name_dr"=> "معاونیت تخنیکی",
              "name_en"=> "Technical Deputy",
              "name_pa"=> "معاونیت تخنیکی",
              "type"=> "1",
              "parent_id"=> "66",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "421",
              "name_dr"=> "ریاست تجدید و انکشاف سیستم ها و پروسه ها",
              "name_en"=> "Department of Renewal and Development of Systems and Processes",
              "name_pa"=> "ریاست تجدید و انکشاف سیستم ها و پروسه ها",
              "type"=> "1",
              "parent_id"=> "420",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "423",
              "name_dr"=> "ریاست نظارت و ارزیابی",
              "name_en"=> "Monitoring and Evaluation Directorate",
              "name_pa"=> "ریاست نظارت و ارزیابی",
              "type"=> "1",
              "parent_id"=> "420",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "425",
              "name_dr"=> "ریاست مطالعات و پالیسی ",
              "name_en"=> "Directorate of Studies and Policy",
              "name_pa"=> "ریاست مطالعات و پالیسی ",
              "type"=> "1",
              "parent_id"=> "420",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "426",
              "name_dr"=> "اداره ثبت و بررسی دارایی ها",
              "name_en"=> "The Office for the Registration of Property",
              "name_pa"=> "اداره ثبت و بررسی دارایی ها",
              "type"=> "1",
              "parent_id"=> "420",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "427",
              "name_dr"=> "ریاست عمومی دفتر مقام عالی ریاست جمهوری",
              "name_en"=> "Office of the Cheif of Staff to the President",
              "name_pa"=> "ریاست عمومی دفتر مقام عالی ریاست جمهوری",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "428",
              "name_dr"=> "معاونیت پالیسی, نظارت و بررسی",
              "name_en"=> "Office of the Deputy Chief of Staff for Policy, Monitoring and Evaluation",
              "name_pa"=> "معاونیت پالیسی, نظارت و بررسی",
              "type"=> "1",
              "parent_id"=> "427",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "429",
              "name_dr"=> "معاونیت ارتباطات استراتیژیک و رسانه ها",
              "name_en"=> "Office of the Deputy Chief of Staff for Strategic Communications and Media",
              "name_pa"=> "معاونیت ارتباطات استراتیژیک و رسانه ها",
              "type"=> "1",
              "parent_id"=> "427",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "430",
              "name_dr"=> "معاونیت منابع و اداره",
              "name_en"=> "Office of the Deputy Chief of Staff for Resources and Administration",
              "name_pa"=> "معاونیت منابع و اداره",
              "type"=> "1",
              "parent_id"=> "427",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "431",
              "name_dr"=> "ریاست تشریفات",
              "name_en"=> "Directorate of Protocol",
              "name_pa"=> "ریاست تشریفات",
              "type"=> "1",
              "parent_id"=> "427",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "433",
              "name_dr"=> "ریاست پالیسی, نظارت و ارزیابی",
              "name_en"=> "Directorate of Policy, Monitoring and Evaluation ",
              "name_pa"=> "ریاست پالیسی, نظارت و ارزیابی",
              "type"=> "1",
              "parent_id"=> "428",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "434",
              "name_dr"=> "ریاست تفتیش و بررسی",
              "name_en"=> "Directorate of Audit and Investigation",
              "name_pa"=> "ریاست تفتیش و بررسی",
              "type"=> "1",
              "parent_id"=> "428",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "435",
              "name_dr"=> "سکرتریت دولتداری باز",
              "name_en"=> "Secretariat of Open Government Partnership",
              "name_pa"=> "سکرتریت دولتداری باز",
              "type"=> "1",
              "parent_id"=> "428",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "436",
              "name_dr"=> "ریاست تعقیب اوامر وهدایات",
              "name_en"=> "Directorate of Commandments Coordination and Follow-up",
              "name_pa"=> "ریاست تعقیب اوامر وهدایات",
              "type"=> "1",
              "parent_id"=> "428",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "437",
              "name_dr"=> "ریاست امور حقوقی",
              "name_en"=> "Directorate of Legal Affairs",
              "name_pa"=> "ریاست امور حقوقی",
              "type"=> "1",
              "parent_id"=> "428",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "438",
              "name_dr"=> "سخنگوی مقام عالی ریاست جمهوری",
              "name_en"=> "Spokesperson to The President",
              "name_pa"=> "سخنگوی مقام عالی ریاست جمهوری",
              "type"=> "1",
              "parent_id"=> "493",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "439",
              "name_dr"=> "ریاست مطبوعات و آگاهی عامه",
              "name_en"=> "Directorate of Media and Public Awareness ",
              "name_pa"=> "ریاست مطبوعات و آگاهی عامه",
              "type"=> "1",
              "parent_id"=> "429",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "440",
              "name_dr"=> "ریاست مرکز اطلاعات و رسانه های حکومت",
              "name_en"=> "Directorate of Information Center and Government Media",
              "name_pa"=> "ریاست مرکز اطلاعات و رسانه های حکومت",
              "type"=> "1",
              "parent_id"=> "493",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "442",
              "name_dr"=> "ریاست مالی",
              "name_en"=> "Directorate of Finance",
              "name_pa"=> "ریاست مالی",
              "type"=> "1",
              "parent_id"=> "430",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "443",
              "name_dr"=> "ریاست منابع بشری",
              "name_en"=> "Directorate of Human Resources",
              "name_pa"=> "ریاست منابع بشری",
              "type"=> "1",
              "parent_id"=> "430",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "444",
              "name_dr"=> "ریاست اسناد و ارتباط",
              "name_en"=> "Directorate of Documents & Communications",
              "name_pa"=> "ریاست اسناد و ارتباط",
              "type"=> "1",
              "parent_id"=> "430",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "446",
              "name_dr"=> "مشاور ارشد ریاست جمهوری ا.ا در امور انسجام روابط اقوام در حوزه شمال کشور (حکمت الله توریالی)",
              "name_en"=> "Senior Advisor to the president in Tribal Coordination Affairs",
              "name_pa"=> "مشاور ارشد ریاست جمهوری ا.ا در امور انسجام روابط اقوام در حوزه شمال کشور",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "447",
              "name_dr"=> "مشاوریت ریاست جمهوری ا.ا در امور قومی (محمد جان ایشان روحانی) ",
              "name_en"=> "Advisory to the President in Ethnic Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا.ا در امور قومی",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
            ],
            [
              "id"=> "448",
              "name_dr"=> "مشاوریت ریاست جمهوری ا.ا در امور دینی (مولوی سردار ځدران)",
              "name_en"=> "Advisory to the President in Religious Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا.ا در امور دینی",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "449",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور سرحدات و قبایل (عبدالغفور لیوال)",
              "name_en"=> "Senior Advisor to the president in ‌Borders and Tribs Affairs",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور سرحدات و قبایل",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "450",
              "name_dr"=> "مشاور ارشد ریاست جمهوری اسلامی افغانستان (فضل محمود فضلی)",
              "name_en"=> "Senior Advisor to the President",
              "name_pa"=> "مشاور ارشد ریاست جمهوری اسلامی افغانستان",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "451",
              "name_dr"=> "ریاست نظارت ار مراحل تدارکات و تصدیق ظرفیت",
              "name_en"=> "Monitoring of Procurement Processes and Capacity Apporval Directorate",
              "name_pa"=> "ریاست نظارت ار مراحل تدارکات و تصدیق ظرفیت",
              "type"=> "1",
              "parent_id"=> "404",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "452",
              "name_dr"=> "ریاست نظارت از پیشرفت قراردادها",
              "name_en"=> "Monitoring of Contract Advancement Dirctorate",
              "name_pa"=> "ریاست نظارت از پیشرفت قراردادها",
              "type"=> "1",
              "parent_id"=> "404",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "453",
              "name_dr"=> "ریاست تشویق و حمایت از سرمایه گذاری در قراردادهای عامه",
              "name_en"=> "Persuative and supportive of investment in public contract Directorate",
              "name_pa"=> "ریاست تشویق و حمایت از سرمایه گذاری در قراردادهای عامه",
              "type"=> "1",
              "parent_id"=> "404",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "454",
              "name_dr"=> "ریاست تصدیق ظرفیت سکتور خصوصی",
              "name_en"=> "Directorate of Private Sector Capacity Approval",
              "name_pa"=> "ریاست تصدیق ظرفیت سکتور خصوصی",
              "type"=> "1",
              "parent_id"=> "404",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "455",
              "name_dr"=> "ریاست تحلیل پروژه ها و انکشاف برنامه ها",
              "name_en"=> "Directorate of Projects Analyse and Programms Development",
              "name_pa"=> "ریاست تحلیل پروژه ها و انکشاف برنامه ها",
              "type"=> "1",
              "parent_id"=> "405",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "456",
              "name_dr"=> "ریاست تدارکات اقلام عامه",
              "name_en"=> "Directorate of Procuerment",
              "name_pa"=> "ریاست تدارکات اقلام عامه",
              "type"=> "1",
              "parent_id"=> "405",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "457",
              "name_dr"=> "ریاست پالیسی تدارکات",
              "name_en"=> "Directorate of Procuerment Policy",
              "name_pa"=> "ریاست پالیسی تدارکات",
              "type"=> "1",
              "parent_id"=> "58",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "458",
              "name_dr"=> "ریاست تسهیلات تدارکاتی",
              "name_en"=> "Directorate of Procuerment Facilities",
              "name_pa"=> "ریاست تسهیلات تدارکاتی",
              "type"=> "1",
              "parent_id"=> "405",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "459",
              "name_dr"=> "ریاست انستیتوت تدارکات ملی",
              "name_en"=> "Directorate of National Procuerment Institute",
              "name_pa"=> "ریاست انستیتوت تدارکات ملی",
              "type"=> "1",
              "parent_id"=> "406",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "460",
              "name_dr"=> "ریاست عملیاتی",
              "name_en"=> "Directorate of Operations",
              "name_pa"=> "ریاست عملیاتی",
              "type"=> "1",
              "parent_id"=> "406",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "461",
              "name_dr"=> " ریاست منابع بشری و امور کدری",
              "name_en"=> "Directorate of Human Resources",
              "name_pa"=> " ریاست منابع بشری و امور کدری",
              "type"=> "1",
              "parent_id"=> "406",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "462",
              "name_dr"=> "ریاست دفتر ریاست عمومی اداره تدارکات ملی",
              "name_en"=> "Office of the National Procuerment authority",
              "name_pa"=> "ریاست دفتر ریاست عمومی اداره تدارکات ملی",
              "type"=> "1",
              "parent_id"=> "58",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "463",
              "name_dr"=> "ریاست دارالانشای کمیسیون تدارکات ملی",
              "name_en"=> "National Procurement Commission Secretariat Directorate   ",
              "name_pa"=> "ریاست دارالانشای کمیسیون تدارکات ملی",
              "type"=> "1",
              "parent_id"=> "58",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "464",
              "name_dr"=> "ریاست تامین شفافیت",
              "name_en"=> "Directorate of Integrity Transparency",
              "name_pa"=> "ریاست تامین شفافیت",
              "type"=> "1",
              "parent_id"=> "58",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "465",
              "name_dr"=> "ریاست ارتباطات استراتیژیک",
              "name_en"=> "Directorate of Strategic Communications",
              "name_pa"=> "ریاست ارتباطات استراتیژیک",
              "type"=> "1",
              "parent_id"=> "58",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "466",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور قومی و اجتماعی (صادق منگل)",
              "name_en"=> "Advisor to the President in Tribal and Social Affairs",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور قومی و اجتماعی",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "467",
              "name_dr"=> "سکرتریت ویژه مبارزه علیه فساد اداری",
              "name_en"=> "Special Anti-Corruption Secretariat",
              "name_pa"=> "سکرتریت ویژه مبارزه علیه فساد اداری",
              "type"=> "1",
              "parent_id"=> "428",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "468",
              "name_dr"=> "معاونیت خدماتی و حمایوی",
              "name_en"=> "Supportive and Services Deputy",
              "name_pa"=> "معاونیت خدماتی و حمایوی",
              "type"=> "1",
              "parent_id"=> "408",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "469",
              "name_dr"=> "معاونیت عملیاتی و تخنیکی",
              "name_en"=> "Operation and Technical Deputy",
              "name_pa"=> "معاونیت عملیاتی و تخنیکی",
              "type"=> "1",
              "parent_id"=> "408",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "470",
              "name_dr"=> "ریاست انسجام امور ساختمانی و آبدات تاریخی",
              "name_en"=> "Directorate of Coordination of Construction Affairs and Historical Monuments",
              "name_pa"=> "ریاست انسجام امور ساختمانی و آبدات تاریخی",
              "type"=> "1",
              "parent_id"=> "469",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "471",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری اسلامی افغانستان(انجینیر محمدامین کریم)",
              "name_en"=> "Senior Advisory to the Presidency",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری اسلامی افغانستان",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "472",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور تحقیق و توسعه زیربناها(شادمحمد حرگند)",
              "name_en"=> "Senior Advisory to the President in Research and Infrastructures Affairs",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور تحقیق و توسعه زیربناها",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "473",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور مردمی و سیاسی(ضیاالحق امرخیل)",
              "name_en"=> "Advisory to the President in Public and Political Affairs",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور مردمی و سیاسی",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "474",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور توسعه شهری (احمد ذکی سرفراز)",
              "name_en"=> "Senior Advisor to the president in ‌Urban Development",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور توسعه شهری",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "475",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور ترانسپورت(فرید احمد مومند)",
              "name_en"=> "Senior Advisor to the president in Transport Affairs",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور ترانسپورت",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "476",
              "name_dr"=> "مشاوریت ریاست جمهوری ا.ا در امور مطبوعات (پوهان تاج محمد زریر)",
              "name_en"=> "Advisory to the president in Media Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا.ا در امور مطبوعات",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "477",
              "name_dr"=> "مشاوریت ریاست جمهوری ا.ا در امور قومی(جمعه خان احکزی)",
              "name_en"=> "Advisory to the President in Religious Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا.ا در امور قومی",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "478",
              "name_dr"=> "ریاست امور ولایات",
              "name_en"=> "Directorate of Provinces Affairs",
              "name_pa"=> "ریاست امور ولایات",
              "type"=> "1",
              "parent_id"=> "420",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "479",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور اجتماعی(عبدالودود ثابت)",
              "name_en"=> "Senior Advisor to the president in Social Affairs",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور اجتماعی",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "480",
              "name_dr"=> "نماینده خاص ریاست جمهوری ا.ا در امور منطقوی برای اجماع صلح(محمد عمر داوودزی)",
              "name_en"=> " Special Envoy to the President in Regional Affairs for Peace",
              "name_pa"=> "نماینده خاص ریاست جمهوری ا.ا در امور منطقوی برای اجماع صلح",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "481",
              "name_dr"=> "معاونیت انسجام امور مردمی",
              "name_en"=> "Deputy Chief of Staff for Public Events Coordination",
              "name_pa"=> "معاونیت انسجام امور مردمی",
              "type"=> "1",
              "parent_id"=> "427",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "482",
              "name_dr"=> "ریاست تنظیم برنامه ها",
              "name_en"=> "Events Management Directorate",
              "name_pa"=> "ریاست تنظیم برنامه ها",
              "type"=> "1",
              "parent_id"=> "481",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "483",
              "name_dr"=> "ریاست انسجام برنامه های فرهنگی, سیاسی و اجتماعی",
              "name_en"=> "Directorate of Cultural, Political and Social Events Coordination",
              "name_pa"=> "ریاست انسجام برنامه های فرهنگی, سیاسی و اجتماعی",
              "type"=> "1",
              "parent_id"=> "481",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "484",
              "name_dr"=> "ریاست امور کدری دولت",
              "name_en"=> "Directorate of State Cadre Affairs",
              "name_pa"=> "ریاست امور کدری دولت",
              "type"=> "1",
              "parent_id"=> "430",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "485",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور اهل هنود و سیک های افغانستان",
              "name_en"=> "Senior Advisor to the president in hindus affairs",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور اهل هنود و سیک های افغانستان",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "486",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور قومی و مردمی (محمد اقبال عزیزی)",
              "name_en"=> "Senior Advisor to the president in Religious and Public Affairs",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور قومی و مردمی",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "487",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور اجتماعی(عبدالرشید ایوبی)",
              "name_en"=> "Senior Advisor to the president in Social Affairs",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور اجتماعی",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "488",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور مردمی (جنرال احمدخان رحیمی غوری)",
              "name_en"=> "Senior Advisor to the president in Public Affairs",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور مردمی",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "489",
              "name_dr"=> "ریاست طرح و دیزاین امور ساختمانی",
              "name_en"=> "Directorate of Design of Construction Affairs",
              "name_pa"=> "ریاست طرح و دیزاین امور ساختمانی",
              "type"=> "1",
              "parent_id"=> "469",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "490",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور اجتماعی (فیض محمد عثمانی)",
              "name_en"=> "Senior Advisor to the president in Social Affairs",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور اجتماعی",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "491",
              "name_dr"=> "مشاوریت ریاست جمهوری ا.ا در امور فرهنگی و اجتماعی (محمد سالم حسنی)",
              "name_en"=> "Senior Advisor to the President in Cultur and Social Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا.ا در امور فرهنگی و اجتماعی",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "492",
              "name_dr"=> "مشاوریت ریاست جمهوری ا.ا در امور انسجام علما (مولوی رحیم الله عزیزی)",
              "name_en"=> "Advisor to the President in Olama Coordination Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا.ا در امور انسجام علما",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "493",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور روابط عامه و استراتژیک (وحید عمر)",
              "name_en"=> "Senior Advisor to the President in Public and Strategic Affairs",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور روابط عامه و استراتژیک",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "494",
              "name_dr"=> "واحد دستیار خاص رئیس جمهوری ا.ا",
              "name_en"=> "President special Assistance unit",
              "name_pa"=> "واحد دستیار خاص رئیس جمهوری ا.ا",
              "type"=> "1",
              "parent_id"=> "66",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "495",
              "name_dr"=> "ریاست تفتیش داخلی",
              "name_en"=> "Directorate of Internal Audit",
              "name_pa"=> "ریاست تفتیش داخلی",
              "type"=> "1",
              "parent_id"=> "66",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "496",
              "name_dr"=> "ریاست حمایت از سرمایه گذاری و انسجام پروژه های ملی",
              "name_en"=> "Directorate of Investment Support and  National Projects Coherence",
              "name_pa"=> "ریاست حمایت از سرمایه گذاری و انسجام پروژه های ملی",
              "type"=> "1",
              "parent_id"=> "420",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "497",
              "name_dr"=> "ریاست انکشاف سیستم تدارکات الکترونیکی و باز افغانستان",
              "name_en"=> "Directorate of E-Procurement Development",
              "name_pa"=> "ریاست انکشاف سیستم تدارکات الکترونیکی و باز افغانستان",
              "type"=> "1",
              "parent_id"=> "404",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "498",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا.ا در زمینه نوآوری - داکتر عبدالسمیع حامد",
              "name_en"=> "Senior Advisory to the President in Innovation Affairs",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا.ا در زمینه نوآوری",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "499",
              "name_dr"=> "مشاوریت ریاست جمهوری ا.ا در امور فرهنگی - محمد زرین انحور",
              "name_en"=> "Advisory to the President in Cultural Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا.ا در امور فرهنگی",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "501",
              "name_dr"=> "مشاوریت ریاست جمهوری ا.ا در امور مردمی (عبدالرحیم کریمی)",
              "name_en"=> "Advisory to the president in Public Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا.ا در امور مردمی",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "502",
              "name_dr"=> "مشاوریت ریاست جمهوری ا.ا در امور اجتماعی (عبدالحکیم نجرابی)",
              "name_en"=> "Advisory to the president in Social Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا.ا در امور اجتماعی",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "504",
              "name_dr"=> "ریاست تعقیب تعهدات ملی و بین المللی",
              "name_en"=> "National and International Obligations Follow up Directorate",
              "name_pa"=> "ریاست تعقیب تعهدات ملی و بین المللی",
              "type"=> "1",
              "parent_id"=> "420",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "505",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور همبستگی اجتماعی و رفاه اقلیت ها (صاحب نظر سنگین)",
              "name_en"=> "Senior Advisory in Social Solidarity and Minorities Welfare Affairs to the president",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور همبستگی اجتماعی و رفاه اقلیت ها",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "506",
              "name_dr"=> "مشاوریت ریاست جمهوری ا.ا در امور فرهنگی (غلام غوث)",
              "name_en"=> "President’s Advisory in cultural Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا.ا در امور فرهنگی غلام غوث",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "507",
              "name_dr"=> "مشاوریت ریاست جمهوری ا.ا در امور مردمی حوزه شرق ( جاوید زمان)",
              "name_en"=> "Advisory to the President on Public Affairs for East Zone",
              "name_pa"=> "مشاوریت ریاست جمهوری ا.ا در امور مردمی حوزه شرق  جاوید زمان",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "508",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امورمردمی حوزه جنوب (محمد عثمان رحمانی)",
              "name_en"=> "Senior Advisory in Public Affairs south zone to the president",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امورمردمی حوزه جنوب محمد عثمان رحمانی",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "509",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور اجتماعی و مردمی (نجیب الله کابلی)",
              "name_en"=> "Senior Advisory to the President in Social and Public Affairs",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور اجتماعی و مردمی نجیب الله کابلی",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "510",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور رسیدگی به مسایل مردمی (ریاض احمد حیات)",
              "name_en"=> "",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور رسیدگی به مسایل مردمی ریاض احمد حیات",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "511",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا.ا در حکومتداری خوب (عبدالحی نعمتی)",
              "name_en"=> "Senior Advisory to the president in Good Governance",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا.ا در حکومتداری خوب عبدالحی نعمتی",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "512",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور تحصیلات عالی (نجیب الله خواجه عمری)",
              "name_en"=> "",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور تحصیلات عالی نجیب الله خواجه عمری",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "513",
              "name_dr"=> "ریاست تصدی ساختمانی بنایی",
              "name_en"=> "Banayee  Construction  Enterprise  Directorate",
              "name_pa"=> "ریاست تصدی  ساختمانی بنایی",
              "type"=> "1",
              "parent_id"=> "469",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "514",
              "name_dr"=> "مشاوریت ریاست جمهوری ا. ا در امور دینی وانسجام  علمآ(مولوی احمد شاه شاکر)",
              "name_en"=> "Advisory to the President in Ulama Coherence",
              "name_pa"=> "مشاوریت ریاست جمهوری ا. ا در امور دینی وانسجام  علمآ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "516",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور فرهنګی (شاه حسین مرتضوی(",
              "name_en"=> "",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور فرهنګی ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "517",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور قومی (محمد یونس هوتک)",
              "name_en"=> "",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور قومی ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "518",
              "name_dr"=> "مشاوریت ریاست جمهوری ا.ا در امور توسعه سرمایه گذاری اکسپودبی(داود جبارخیل) ۲۰۲۰ ",
              "name_en"=> "Advisory to the President 2020 Dubai Expo Investment Development Affairs",
              "name_pa"=> "مشاوریت ریاست جمهوری ا.ا در امور توسعه سرمایه گذاری اکسپودبی",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "519",
              "name_dr"=> "معاونیت حکومتداری و برنامه ها",
              "name_en"=> "",
              "name_pa"=> "معاونیت حکومتداری و برنامه ها",
              "type"=> "1",
              "parent_id"=> "427",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "520",
              "name_dr"=> "ریاست همآهنگی شورا های عالی ریاست جمهوری",
              "name_en"=> "",
              "name_pa"=> "ریاست همآهنگی شورا های عالی ریاست جمهوری",
              "type"=> "1",
              "parent_id"=> "519",

              "deleted_at"=> NULL,
              ],
            [
              "id"=> "521",
              "name_dr"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور مردمی (اعتبار خان(",
              "name_en"=> "",
              "name_pa"=> "مشاوریت ارشد ریاست جمهوری ا.ا در امور مردمی ",
              "type"=> "1",
              "parent_id"=> "80",

              "deleted_at"=> NULL
            ]
        ];

        $faker = Faker\Factory::create('fa_IR');
        foreach($departments as $item) {
            Department::create($item);
        }
    }
}
