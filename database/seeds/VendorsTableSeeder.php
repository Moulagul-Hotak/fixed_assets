<?php

use Illuminate\Database\Seeder;
use App\Vendor;

class VendorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Vendor::truncate();
        $faker = Faker\Factory::create('fa_IR');
        for($i = 0; $i < 20 ; $i++) {
            Vendor::create(
                [
                    'name_dr' => $faker->name,
                    'description' => $faker->realText($maxNbChars = 200, $indexSize = 2),
                ]
            );
        }
    }
}
