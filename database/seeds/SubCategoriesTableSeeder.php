<?php

use Illuminate\Database\Seeder;
use App\SubCategory;

class SubCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SubCategory::truncate();
        $faker = Faker\Factory::create('fa_IR');
        for($i = 0; $i < 20 ; $i++) {
            SubCategory::create(
                [
                    'name_dr' => $faker->name,
                    'description' => $faker->realText($maxNbChars = 200, $indexSize = 2),
                    'main_category_id' => $faker->numberBetween($min = 1, $max = 20),
                ]
            );
        }
    }
}
