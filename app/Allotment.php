<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Fico7489\Laravel\Pivot\Traits\PivotEventTrait;

class Allotment extends Model
{
    use LogsActivity;
    use SoftDeletes;
    use PivotEventTrait;

    protected $table = 'allotments';

    protected $fillable = ['receiver_employee_id','employee_type','details','item_details_specifications_id','item_balances_id','item_details_id' ,'allotment_date','details','return_date','total_alloted','fece5_id','condition', 'status', 'created_by', 'updated_by', 'deleted_by'];

    protected static $logAttributes = ['receiver_employee_id','employee_type','details','item_details_specifications_id','item_balances_id','item_details_id','allotment_date','details','return_date','total_alloted','fece5_id', 'condition', 'status', 'created_by', 'updated_by', 'deleted_by'];

    public function allotment() {
        return $this->belongsTo('App\Allotment');
    }

    public function unit(){
        return $this->belongsTo('App\Unit');
    }

    public function itemBalance() {
        return $this->belongsTo('App\ItemBalance','item_balances_id','id');
    }

    public function department() {
        return $this->belongsTo('App\Department');
    }

    public function distributerEmployee() {
        return $this->belongsTo('App\Employee', 'distributer_employee_id', 'id');
    }

    public function receiverEmployee() {
        return $this->belongsTo('App\Employee', 'receiver_employee_id', 'id');
    }


    public function fece9() {
        return $this->belongsTo('App\Fece9');
    }
    public function motamed()
    {
        return $this->belongsTo('App\Motamed','receiver_employee_id','employee_id');
    }
   
    // public function itemDetailsSpecifications() {
    //     return $this->belongsTo('App\ItemDetailsSpecifications', 'item_details_specifications_id', 'id');
    // }

    public function itemDetailsSpecifications() {
        return $this->belongsTo('App\ItemDetailsSpecifications', 'item_details_specifications_id', 'id');
    }

    public function employee() {
        return $this->belongsTo('App\Employee', 'receiver_employee_id', 'id');
    }
    public function externalEmployee() {
        return $this->belongsTo('App\ExternalEmployee', 'receiver_employee_id', 'id');
    }
    public function scopeOfEmployee($query, $value) {
        if($value) {
          $query->where('receiver_employee_id', $value);
        }
        return $query;
      }
      
    public function scopeOfCreated($query, $value) {
        if($value) {
          $query->where('created_by', $value);
        }
        return $query;
      }

    //   public function scopeOfDepartment($query, $value) {
    //     if($value) {
    //       $query->where('id', $value);
    //     }
    //     return $query;
    //   }

      

    public static function boot() {
        parent::boot();
        static::pivotAttaching(function ($model, $relationName, $pivotIds, $pivotIdsAttributes) {
            if($relationName == 'itemDetails') {
                activity()
                ->performedOn($model)
                ->causedBy(auth()->user())
                ->withProperties($pivotIdsAttributes)
                ->log($relationName);}
        });
        // create a event to happen on updating
        static::updating(function($table)  {
            $table->updated_by = Auth::user()->id ;
        });

        // create a event to happen on deleting
        static::deleting(function($table) {
            $table->deleted_by = Auth::user()->id ;
        });

        // create a event to happen on saving
        static::saving(function($table)  {
            $table->created_by = Auth::user()->id ;
        });
    }
}
