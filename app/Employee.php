<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Employee extends Model
{
    public function endItemDetails() {
        return $this->belongsToMany('App\ItemDetails', 'employee_end_item_details')->withPivot('allotment_date', 'return_date', 'status', 'total_alloted', 'fece5_id', 'condition');
    }

    public function requests() {
        return $this->hasMany('App\Request');
    }

    public function user() {
        return $this->belongsTo('App\User','id','employee_id');
    }

    public function department() {
        return $this->belongsTo('App\Department');
    }
    public function fece5() {
        return $this->hasMany('App\Fece5','distributer_employee_id','id');
    }
    public function meem7() {
        return $this->hasMany('App\Meem7','akhz_az_wajh_employee_id','id');
    }
    public function allotments() {
        return $this->hasMany('App\Allotment','receiver_employee_id','id')->where('employee_type',0);
    }
    public function employee_attachments() {
        return $this->hasMany('App\EmployeeAttachment','employee_id','id');
    }
    public function motamed()
    {
        return $this->hasOne('App\Motamed');
    }
    public static function boot() {
        parent::boot();

        // create a event to happen on updating
        static::updating(function($table)  {
            $table->updated_by = Auth::user()->id ;
        });

        // create a event to happen on deleting
        static::deleting(function($table)  {
            $table->deleted_by = Auth::user()->id ;
        });

        // create a event to happen on saving
        static::saving(function($table)  {
            $table->created_by = Auth::user()->id ;
        });
    }
    public function scopeSearch($query, $name, $father_name, $designation, $department)
    {
        return $query->select('id', 'name_dr', 'father_name_dr', 'current_position_dr', 'department_id', 'last_name')
            ->where('name_dr', 'LIKE', '%' . $name . '%')
            ->where('father_name_dr', 'LIKE', '%' . $father_name . '%')
            ->where('current_position_dr', 'LIKE', '%' . $designation . '%')
            ->whereHas('department', function ($q) use ($department) {
                $q->where('name_dr', 'LIKE', '%' . $department . '%');
            });
    }
}
