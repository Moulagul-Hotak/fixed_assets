<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Fece2;
use App\ItemDetailsSpecifications;
use App\SubCategory;
use App\Fece2ItemDetails;
use App\Department;
use App\Allotment;
use App\Http\Requests\StoreFece2Request;

class Fece2Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fece2s = Fece2::orderBy('id', 'desc')->get();
        return view('fece2s.index', compact('fece2s'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() 
    {
        $departments = Department::all();
        $item_details = SubCategory::all();      
        return view('fece2s.create', compact('departments','item_details'));   
    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreFece2Request $request)
    {
        $path = 'storage/uploads/fece2_request/';
       
        $fece2 = Fece2::create($request->all());
        $fece2_item_details = Fece2ItemDetails::create(['fece2_id' => $fece2->id, 'item_details_id' => $request->item_details_id,
        'item_details_specifications_id' => $request->item_details_specifications_id,'sub_category_id'=>$request->sub_category_id]);
        $Item_details_specifications = ItemDetailsSpecifications::where('id',$fece2_item_details->item_details_specifications_id)->update(['item_process' =>'1']);
        if($request->image){
            $image_path = singleFileStore($request, $fece2->id, $path);
            $fece2->update(['request_file' => $image_path]);
        }
        return redirect()->route('fece2s.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $fece2=Fece2::find($id);
        return view('fece2s.show',compact('fece2'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    { 
        $fece2=Fece2::find($id);
        $fece2_item_details = Fece2ItemDetails::where('fece2_id',$fece2->id)->first();
        $departments = Department::all();
        $item_details = SubCategory::all();   
        return view('fece2s.edit',compact('fece2','departments','item_details','fece2_item_details'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $fece2 = Fece2::find($id);
        $fece2->update($request->all());
        $fece2_item_details = Fece2ItemDetails::where('fece2_id',$fece2->id)->update(['fece2_id' => $fece2->id, 'item_details_id' => $request->item_details_id,
        // 'item_details_specifications_id' => $request->item_details_specifications_id,
        'sub_category_id'=>$request->sub_category_id]);
        return redirect()->route('fece2s.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function WaitMeem7 () {
        $fece2s = Fece2::where('file' , '!=' ,'')->where('status',0)->orderBy('id', 'desc')->get();
        return view('fece2s.wait_meem7', compact('fece2s'));
    }
    public function Fece2Confirmed  () { 
        $fece2s = Fece2::where('status' ,'1')->orderBy('id', 'desc')->get();
        return view('fece2s.confirmed', compact('fece2s'));
    }
    public function attachFileAndSend(Request $request)
    {
        $path = 'storage/uploads/fece2/';
        $fece2 = Fece2::find($request->id);
        $image_path = singleFileStore($request, $request->id, $path);
        $fece2->update(['file' => $image_path]);
        return redirect()->back()->with('success', 'فایل پروسس شده موفقانه اضافه گردید.');
    }
    public function attachFileMeem7(Request $request)
    {
        $path = 'storage/uploads/fece2/';
        $fece2 = Fece2::find($request->id);
        $image_path = singleFileStore($request, $request->id, $path);
        $fece2->update(['meem7' => $image_path]);
        return redirect()->back()->with('success', 'فایل  م۷ موفقانه اضافه گردید.');
    }
    public function fece2Confirmation ($id,$status) {
        $fece2=Fece2::find($id);
        $fece2_item_details=Fece2ItemDetails::where('fece2_id',$id)->first();
        $item_details_specification=ItemDetailsSpecifications::where('id',$fece2_item_details->item_details_specifications_id)->update(['status'=>'3']);
        $fece2->update(['status' => $status]);
        return redirect()->route('fece2s.index');
    }
    public function rejectFece2(Request $request ,$id){

        $fece2=Fece2::find($id);
        $fece2->update(['reject_remark' => $request->reject_remark, 'status' => 2]);
        $fece2_item_details = Fece2ItemDetails::where('fece2_id',$fece2->id)->first();
        $Item_details_specifications = ItemDetailsSpecifications::where('id',$fece2_item_details->item_details_specifications_id)->update(['item_process' =>'0']);
        return redirect()->route('fece2s.index')->with('success', trans('global.create_successful'));
  
    }
    public function replaceAttachedFile(Request $request){
        $fece2 = Fece2::find($request->id);
        $path = 'storage/uploads/fece2/';
        $image_path = singleFileStore($request,$request->id,$path); 
        $fece2->update(['file' => $image_path]);
        return redirect()->back()->with('success', 'فایل پروسس شده موفقانه تصحیح گردید.');

    }

}
