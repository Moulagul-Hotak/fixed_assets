<?php

namespace App\Http\Controllers;

use App\Allotment;
use App\Contract;
use App\ContractItemBalance;
use App\ContractItems;
use App\Meem7;
use App\Fece9;
use App\Department;
use App\Employee;
use App\Http\Requests\Meem7Request;
use App\ItemBalance;
use App\Meem7Fece9;
use App\SubCategoriesKey;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Symfony\Component\VarDumper\Caster\MemcachedCaster;

class Meem7Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      
        //get fece9 that at lest one item dosenot exist;
        $fece9s = Fece9::with('department')->whereHas('itemDetails', function ($query) {
              $query->where('status', '0');
          })->whereColumn('created_at', '!=',  'updated_at')->orderBy('id', 'desc')->get();

          $ghair_zakhirawi_fece9 = Fece9::with('department')->whereFece9Type('2')->select(
            [
                DB::raw('count(id) as `count`'),
                DB::raw('MONTH(date) month'),
                DB::raw('YEAR(date) year'),
                'department_id',         
            ]
            )
            ->groupby(['month', 'department_id','year'])
            ->get();

          
        return view('meem7.index',compact('fece9s','ghair_zakhirawi_fece9'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Request\Meem7Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Meem7Request $request)
    {
        // return $request->all();
        
        if(isset($request->masrafi)){
            $request['status'] = 1;
            $contract_id = $request->contract_id;
        
            $meem7 = Meem7::create($request->all());
        //    return $meem7;

            //Ghair Zakhirawi tohidi
            if($request->fece9_type == 2){
                $month = explode('-',$request->date)[1];
                $fece9s = Fece9::
                                whereFece9Type($request->fece9_type)
                                ->where('department_id',$request->department_id)
                                ->where(DB::raw('MONTH(date)'),$month)
                                ->get();

                foreach($fece9s as $fece9){
                   
                    $meem7_fece9 = [
                        'meem7_id'  => $meem7->id,
                        'fece9_id' => $fece9->id,
                    ];

                    // Meem7Fece9::create($meem7_fece9);

                    foreach ($fece9->itemDetails as $item){
                        // divided item from contract
                        $contract_item = ContractItems::where('contract_id',$contract_id)->where('item_details_id',$item->id)->first();

                       $new_amount = $contract_item->available_amount - $item->pivot->quantity;
                       
                       $contract_item->update(['available_amount' => $new_amount]);
        

                        $allotment = [
                            'item_details_id' => $item->id,
                            'receiver_employee_id' => $request->mahal_taslimi,
                            'allotment_date' => $request->date,
                            'meem7_id'  => $meem7->id,
                            'total_alloted' => $item->pivot->quantity,
                            'contract_id'   => $contract_id,
                            'condition' => '1',
                            'status' => '1',
                        ];

                        
        
                        //allot to new user
                        Allotment::create($allotment);
        
        
                        $contract_item_balance = [
                            'item_details_id' => $item->id,
                            'fece9_id'  => $fece9->id,
                            'meem7_id'  => $meem7->id,
                            'contract_id'   => $contract_id,
                            'quantity'  => $item->pivot->quantity
                        ];
                        ContractItemBalance::create($contract_item_balance);
                    }
                    //fece9 Completed
                    $fece9->update(['status'=>'2']);
                }
                                
                return redirect()->route('meem7.show', $meem7->id);

            }else{

                $fece9 = Fece9::find($request->fece9_id);
                $fece9->update(['status'=>'2']);
                $item_details = $fece9->itemDetails;
                foreach ($item_details as $item){
                    // divided item from contract
                   $contract_item = ContractItems::where('contract_id',$contract_id)->where('item_details_id',$item->id)->first();
                   $new_amount = $contract_item->available_amount - $item->pivot->quantity;
                   $contract_item->update(['available_amount' => $new_amount]);
    
                    //Allot to motamed
    
                    $item_balance = [
                        'item_details_id'   => (int)$item->id,
                        'item_amount' => (int)$item->pivot->quantity,
                        'type'  => '1',
                        'receiver_employee_id' =>(int)$request->mahal_taslimi,
                        'created_by'    =>Auth::user()->id,
                    ];

                    $inserted_itme_balance = ItemBalance::create($item_balance);
                            
                    $allotment = [
                        'item_details_id' => (int)$item->id,
                        'receiver_employee_id' => (int)$request->mahal_taslimi,
                        'allotment_date' => $request->date,
                        'meem7_id'  => (int)$meem7->id,
                        'condition' => '1',
                        'status' => '1',
                        'item_balances_id' => $inserted_itme_balance->id,
                        'total_alloted' => (int)$item->pivot->quantity,
                    ];

    
                    //allot to new user
                    Allotment::create($allotment);
    
    
                    $contract_item_balance = [
                        'item_details_id' => $item->id,
                        'fece9_id'  => $request->fece9_id,
                        'meem7_id'  => $meem7->id,
                        'contract_id'   => $contract_id,
                        'quantity'  => $item->pivot->quantity
                    ];
                    ContractItemBalance::create($contract_item_balance);
                }
    

            } 
            return redirect()->route('meem7.show', $meem7->id);



        }else{

            $path = 'storage/uploads/meem3/';
            $file_name = singleFileStore($request,time(),$path);
            $request['meem3_file']=$file_name;
            $meem7 = Meem7::create($request->all());
            return redirect()->route('meem7.create_meem7', $meem7->fece9_id )->with('success', trans('global.create_successful'));
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Meem7  $meem7
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
          $meem7 = Meem7::find($id);
        
        if($meem7->fece9_id == NULL){
            $meem7_fece9 = Meem7Fece9::with('fece9')->where('meem7_id',$id);
            
            $fece9s = $meem7_fece9->get();
            $number = '';
            $item_details = [];
            foreach($fece9s as $fece9){
                $number .= ''.$fece9->fece9->number.',';
                // return $fece9->fece9->itemDetails[0];
                array_push($item_details,$fece9->fece9->itemDetails[0]); 
            }
            // array_unique($item_details, SORT_REGULAR));
            $department = Department::find($meem7_fece9->first()->fece9->department_id);
            $parameter = [
                'number' => $number,
                'item_details' => $item_details, 
                'department' => $department
            ];

            
            return view('meem7.show1',compact('meem7','meem7_fece9','parameter')); 

        }

        return view('meem7.show',compact('meem7')); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Meem7  $meem7
     * @return \Illuminate\Http\Response
     */
    public function edit(Meem7 $meem7)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Meem7  $meem7
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Meem7 $meem7)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Meem7  $meem7
     * @return \Illuminate\Http\Response
     */
    public function destroy(Meem7 $meem7)
    {
        //
    }

    public function createMeem7($fece9_id){

      $fece9 = Fece9::find($fece9_id);
      $item_details_ids = $fece9->itemDetails->pluck('id');
      $item_contract_ids = ContractItems::whereIn('item_details_id', $item_details_ids)->groupBy('contract_id')->pluck('contract_id');
      $contracts = Contract::whereIn('id',$item_contract_ids)->get();
    //   return $contracts;
      $departments = Department::all();
      $employees = Employee::all();
      $meem7=Meem7::where('fece9_id',$fece9_id)->first();
      return view('meem7.create',compact('fece9','departments','employees','meem7','contracts'));
    }

    public function createMeem7GhairZakhirawi($department_id,$month,$fece9_type){

         $fece9s = Fece9::with(['itemDetails','department'])->whereFece9Type($fece9_type)->where('department_id',$department_id)->whereStatus('1')->where( DB::raw('MONTH(date)'),$month)->get();
         $item_details = [];

         //get Releted Contract
         foreach($fece9s as $fece9){
             $item_details = array_merge($item_details,$fece9->itemDetails->pluck('id')->toArray());
         }
         $item_details_ids = array_unique($item_details);
         $item_contract_ids = ContractItems::whereIn('item_details_id', $item_details_ids)->groupBy('contract_id')->pluck('contract_id');
         $contracts = Contract::whereIn('id',$item_contract_ids)->get();
        //  return $fece9s->first();
         return view('meem7.create',compact('fece9s','fece9_type','contracts'));

    }

    public function getMeem7ItemDetailsSpecifications($meem7_id) {
        $meem7 = Meem7::find($meem7_id);
        return $meem7->itemDetailsSpecifications->groupBy('item_details_id');
    }

    public function getMeem7ExtraSpecifications($meem7_id) {
        $specs = Meem7::find($meem7_id)->itemDetailsSpecifications;
        $arr = [];

        foreach($specs as $item) {
            $temp = $item->subCategoryKeys;
            $temp->name_dr = $item->itemDetails->name_dr;
            $arr[$item->item_details_id] = $temp;
        }
        return $arr;
    }
    public function completed(){
        
        $fece9s = Fece9::whereHas('meem7', function ($query) {
            $query->whereStatus('1');
        })->get();

        return view('meem7.completed',compact('fece9s'));
        
    }
}
