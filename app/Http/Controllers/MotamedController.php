<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use App\MotamedType;
use App\Motamed;
use App\Http\Requests\StoreMotamedRequest;
use App\Http\Requests\UpdateMotamedRequest;


class MotamedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $employees=Employee::all();
        $motamed_types = MotamedType::all();
        return view('motamed.create',compact('employees','motamed_types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMotamedRequest $request)
    {
        $employee=Employee::find($request->employee_id);
       $motamed=new Motamed;
       $motamed->employee_id=$employee->id;
       $motamed->department_id = $employee->department_id;
       $motamed->motamed_type_id =$request->motamed_type_id;
       $motamed->name_dr = $employee->name_dr;
       $motamed->father_name_dr = $employee->father_name_dr;
       $motamed->current_position_dr = $employee->current_position_dr;
       $motamed->last_name = $employee->last_name;
       $saved_motamed= $motamed->save();
       if($saved_motamed){
            return redirect()->route('motamed.create')
            ->with('success',trans('global.motamed_saved'));
        }else{
            return redirect()->route('motamed.create')
            ->with('success',trans('global.error_store_message'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $motamed = Motamed::find(decrypt($id));
        $employees=Employee::all();
        $motamed_types = MotamedType::all();
        return view('motamed.edit',compact('employees','motamed_types','motamed'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateMotamedRequest $request, $id)
    {
        
        $motamed=Motamed::find(decrypt($id));
        $motamed->update($request->input());
        return redirect()->route('allotments.motamed_allotments')->with('success', trans('global.update_successful'));;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function getMotameds () {
        $motameds = Motamed::all();
        return response()->json([
            'motameds' => $motameds,
        ]);
    } 
    public function getMotamedAllotments($employee_id,$status =null){
         $employee=Motamed::find(decrypt($employee_id));
         $items =  $employee->allotments;
        return view('motamed.show', compact('employee','status'));
    }
}
