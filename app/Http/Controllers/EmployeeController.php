<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Motamed;
use App\ExternalEmployee;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        //
    }


    // get all employees
    public function getAllEmployees() {
        return Employee::all();
    }

   // get  employees By Department
    public function getEmployeesByDep($id) {
        // return $id;
        return Employee::where('department_id',$id)->get();
    }
 
    public function ajaxSearch(Request $request) {
        
        $search = $request->searchTerm;
        if($search) {
        $employee = Employee::where('name_dr','LIKE',"%$search%")->select('id', 'name_dr', 'father_name_dr','current_position_dr')->get();
        $data =array();
        foreach ($employee as $item){
            $data[] =array('id'=>$item->id,'text'=>$item->name_dr.' ولد :'.$item->father_name_dr.' -- '.$item->current_position_dr);
        }
        return json_encode($data);
        }
    }
    public function axiosSearch(Request $request) {
        $search = $request->name;    
        if(isset($request->department_id) && $request->department_id !=null){
            
                $department_id = $request->department_id;
                $employee = Employee::where('name_dr','LIKE', "%$search%")
                ->select('id', 'name_dr', 'father_name_dr','last_name','department_id','current_position_dr')
                ->where('department_id','=',$department_id)
                ->with('department')->get();
            
        }else{
            $employee = Motamed::where('name_dr','LIKE', "%$search%")
            ->select('id', 'name_dr', 'father_name_dr', 'employee_id','last_name','department_id','current_position_dr')
            ->with('department')->get();

            // $employee = Employee::where('name_dr','LIKE', "%$search%")
            // ->select('id', 'name_dr', 'father_name_dr','last_name','department_id','current_position_dr')
            // ->with('department')->get();
        }   
        if($search) {
           
            return response()->json([
                'incomplete_results' => true,
                'items' => $employee,
                'total_count' => $employee->count()
            ]);
        }
    }
    public function getEmployeeFatherAxios(Request $request) {
        $search = $request->father_name;    
 
            $employee = Employee::where('father_name_dr','LIKE', "%$search%")
            ->select('id', 'name_dr', 'father_name_dr','last_name','department_id','current_position_dr')
            ->with('department')->get();
  
        if($search) {
           
            return response()->json([
                'incomplete_results' => true,
                'items' => $employee,
                'total_count' => $employee->count()
            ]);
        }
    }

    public function getEmployeesByFather($name,$father_name){
        $employee = Employee::where('name_dr','LIKE', "%$name%")->where('father_name_dr','LIKE', "%$father_name%")
        ->select('id', 'name_dr', 'father_name_dr','last_name','department_id','current_position_dr')
        ->with('department')->get();

        return $employee;
    }


    public function getEmployeesByNameFather($employee_type,$name,$father_name=null){
        // return $father_name;
        if($employee_type == 0){
            $employee = Employee::where('name_dr','LIKE', "%$name%")->where('father_name_dr','LIKE', "%$father_name%")
            ->select('id', 'name_dr', 'father_name_dr','last_name','department_id','current_position_dr')
            ->with('department')->get();
        }
        else if($employee_type == 2){
            $employee = Motamed::where('name_dr','LIKE', "%$name%")->where('father_name_dr','LIKE', "%$father_name%")
            ->select('id', 'name_dr', 'father_name_dr','last_name','department_id','current_position_dr')
            ->with('department')->get();  
        }
        else{
            $employee = ExternalEmployee::where('name_dr','LIKE', "%$name%")->where('father_name_dr','LIKE', "%$father_name%")
            ->select('id', 'name_dr', 'father_name_dr','current_position_dr')
            ->with('external_department')->get();
        }
       

        return $employee;
    }
}
