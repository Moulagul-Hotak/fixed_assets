<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContractTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // $this->name => 'required',
            'name_dr' => 'required',
        ];
    }
    public function messages()
    {

        return [
            'name_dr.required' => 'ضروری میباشد',
            // $this->name . '.required' => 'ضروری میباشد',
            // $this->name . '.unique' => 'نام واحد تکراری میباشد!',

        ];
    }
}
