<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreFece2Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'number' => 'required',
            // 'date' => 'required',
            // 'department_id' => 'required',
            // 'item_details_id' => 'required',
            // 'sub_category_id' => 'required',
            // 'item_details_specifications_id' => 'required',
            // 'summary' => 'required',

        ];
    }

    public function messages()
    {

        return [
            'number.required' => 'نمبر ف س ۲ ضروری میباشد',
            'date.required' => 'تاریخ ف س ۲ ضروری میباشد',
            'department_id.required' => 'شعبه درخواست کننده ضروری میباشد',
            'item_details_id.required' => 'انتخاب جنس ضروری میباشد',
            'sub_category_id.required' => ' کتگوری فرعی ضروری میباشد',
            'item_details_specifications_id.required' => '  مشخصات جنس ضروری میباشد',
            'summary.required' => 'هدایات ضروری میباشد',


        ];
    }
}
