<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReturnItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'number' => 'required',
            'date' => 'required',
            'description' => 'required',

        ];
    }

    public function messages()
    {

        return [
            'number.required' => 'نمبر ف درخواست ضروری میباشد',
            'date.required' => 'تاریخ ف درخواست ضروری میباشد',
            'description.required' => 'توضیحات ضروری میباشد',
        ];
    }
}
