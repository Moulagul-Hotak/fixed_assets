<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreExternalEmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'external_department_id'=>'required',
            'name_dr'=>'required',
            'father_name_dr'=>'required',
            'current_position_dr'=>'required',
        ];
    }
    public function messages()
    {

        return [
            'external_department_id.required' => 'نام اداره ضروری میباشد!',
            'name_dr.required' => 'نام  ضروری میباشد!',
            'father_name_dr.required' => 'نام پدر ضروری میباشد!',
            'current_position_dr.required' => ' عنوان وظیفه ضروری میباشد!',

        ];
    }
}
