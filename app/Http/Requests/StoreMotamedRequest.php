<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreMotamedRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'employee_id' => 'required',
            'motamed_type_id' => 'required',
        ];
    }
    public function messages () {
        return [
            'employee_id.required' => 'انتخاب کارمند ضروری میباشد',
            'motamed_type_id.required' => 'نوعیت معتمد ضروری میباشد',
        ];
    }
}
