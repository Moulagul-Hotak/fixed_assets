<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Config;

class PasswordRequest extends FormRequest
{
    public function __construct()
    {
        $this->lang = Config::get('app.locale');
        $this->name = 'name_' . $this->lang;
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           
                'password' => ['required'],
                'new_password' => ['required'],
                'conf_password' => ['same:new_password'],


        ];
    }
    public function messages()
    {

        return [
            'password.required' => 'رمز ضروری میباشد!',
            'password.new_password' => 'رمز جدید ضروری میباشد!',
            'password.conf_password' => 'تکرار رمز جدید ضروری میباشد!',
        ];
    }
}
