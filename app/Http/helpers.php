<?php

use App\Employee;
use App\Http\Controllers\ItemDetailsSpecificationsController;
use App\ItemBalance;
use App\ItemDetailsSpecifications;

function getTabsForMenu($route)
{
    $tabs = array();
    if (Route::is('fece9*')) {
        $tabs = [

            [
                'title' => 'ترتیب',
                'route' => route('fece9s.create'),
            ],
            [
                'title' => 'ف س ۹ ثبت شده',
                'route' => route('fece9s.index'),
            ],
            [
                'title' => 'دریافت شده',
                'route' => route('fece9s.received_create'),
            ],
            [
                'title' => 'تاییدی هیئت معاینه',
                'route' => route('fece9s.inspection_approval'),
            ],
            [
                'title' => 'در حال پروسس',
                'route' => route('fece9s.in_progress'),
            ],
            [
                'title' => 'اجراء شده',
                'route' => route('fece9s.completed'),
            ]

        ];
    }
    if (Route::is('fece2*')) {
        $tabs = [

            [
                'title' => 'ترتیب',
                'route' => route('fece2s.create'),
            ],
            [
                'title' => 'ف س ۲ ثبت شده',
                'route' => route('fece2s.index'),
            ],
            [
                'title' => 'ف س ۲ انتظار به م ۷',
                'route' => route('fece2s.wait_meem7'),
            ],
            [
                'title' => 'ف س ۲ تایید شده',
                'route' => route('fece2s.confirmed'),
            ],


        ];
    }
    if (Route::is('fece5*')) {

        $tabs = [
            // [
            //     'title' => 'ترتیب',
            //     'route' => '#',
            // ],
            // [
            //     'title' => 'چاپ شده',
            //     'route' => '#',
            // ],
            // [
            //     'title' => 'لست',
            //     'route' => '#',
            // ],

        ];
    }
    if (Route::is('item_return_requests*')) {

        $tabs = [
            [
                'title' => 'دریافت شده',
                'route' => route('item_return_requests.index'),
            ],
            [
                'title' => 'در حال اجراء',
                'route' => route('item_return_requests.inprogress'),
            ],
            [
                'title' => 'اجراء شده',
                'route' => route('item_return_requests.completed'),
            ]

        ];
    }
    if (Route::is('meem7*')) {

        $tabs = [
            [
                'title' => 'دریافت شده',
                'route' => route('meem7.index'),
            ],
            [
                'title' => 'اجراء شده',
                'route' => route('meem7.completed'),
            ]

        ];
    }
    if (Route::is('items*')) {
        $tabs = [
            [
                'title' => 'خریداری شده',
                'route' => '#',
            ],
            [
                'title' => 'گدام',
                'route' => '#',
            ],
            [
                'title' => 'درخواستی اعاده اجناس',
                'route' => '#',
            ],
            [
                'title' => 'جمع و قید شده به وجه شخص',
                'route' => '#',
            ],

        ];
    }

    if (Route::is('items_basic_information*') || Route::is('vendors*') || Route::is('main_categories*') ||  Route::is('sub_categories*') ||  Route::is('end_categories*') ||  Route::is('sub_categories_keys*') ||  Route::is('item_details*')) {
        $tabs = [
            // [
            //     'title' => 'کتگوری اصلی',
            //     'route' => route('main_categories.index'),
            // ],
            // [
            //     'title' => 'کتگوری فرعی',
            //     'route' => route('sub_categories.index'),
            // ],
            // [
            //     'title' => 'کتگوری نهایی',
            //     'route' => route('end_categories.index'),
            // ],
            // [
            //     'title' => 'کمپنی',
            //     'route' => route('vendors.index'),
            // ],
            [
                'title' => 'نوعیت مشخصات اجناس',
                'route' => route('sub_categories_keys.index'),
            ],
            [
                'title' => 'جنس',
                'route' => route('item_details.index'),
            ],

        ];
    }


    if (Route::is('requested_items.index') || Route::is('requested_items_accept') || Route::is('requested_items_reject')) {
        $tabs = [
            [
                'title' => 'درخواست شده',
                'route' => 'requested_items',
            ],
            [
                'title' => 'اضافه شده',
                'route' => 'requested_items_accept',
            ],
            [
                'title' => 'رد شده',
                'route' => 'requested_items_reject',
            ],


        ];
    }
    if (Route::is('allotments.get_employee_allotments')) {
        $currentURL = $_SERVER['REQUEST_URI'];
        $parts = explode('/', $currentURL);

        $id = $parts[count($parts) - 2]; // Extract the second-to-last part (id)
        $type = $parts[count($parts) - 1]; // Extract the first-to-last part (type)
        $tabs = [
            [
                'title' => 'موجود ',
                'route' => route('allotments.get_employee_allotments', ['id' => $id, 'type' => $type, 'status' => '1']),
            ],
            [
                'title' => 'تاریخچه ',
                'route' => route('allotments.get_employee_allotments', ['id' => $id, 'type' => $type, 'status' => '0']),
            ],



        ];
    }
    if (Route::is('allotments.get_motamed_allotments')) {
        $currentURL = $_SERVER['REQUEST_URI'];
        $pattern = '/allotments\/get_motamed_allotments\/(\w+)/';
        preg_match($pattern, $currentURL, $matches);
        $id = $matches[1];
        $tabs = [
            [
                'title' => 'موجود ',
                'route' => route('allotments.get_motamed_allotments', ['id' => $id, 'status' => '1']),
            ],
            [
                'title' => 'تاریخچه ',
                'route' => route('allotments.get_motamed_allotments', ['id' => $id, 'status' => '0']),
            ],



        ];
    }
    if (Route::is('allotments.get_employees_allotments') || Route::is('external_employee.index')) {
        $tabs = [
            [
                'title' => 'داخلی',
                'route' => route('allotments.get_employees_allotments'),
                // 'route' => 'get_employees_allotments',
            ],
            [
                'title' => 'بیرونی',
                'route' => route('external_employee.index'),
            ],


        ];
    }

    if (Route::is('user_information*')) {
        $tabs = [
            [
                'title' => 'کارمند',
                'route' => '#',
            ],
            [
                'title' => 'یوزر',
                'route' => '#',
            ],
            [
                'title' => 'نقش',
                'route' => '#',
            ],
            [
                'title' => 'صلاحیت',
                'route' => '#',
            ],

        ];
    }
    if (Route::is('reports*')) {
        $tabs = [
            [
                'title' => 'تاریخچه',
                'route' => '#',
            ],

        ];
    }

    return $tabs;
}

//get total available

function getTotal($id)
{
    $item_details_specifications = ItemDetailsSpecifications::where('item_details_id', $id)->get();

    // if item be jam jam
    if (isset($item_details_specifications->first()->subCategoryKeys) && $item_details_specifications->first()->subCategoryKeys->distribution_type) {
        $item_blance = ItemBalance::where('item_details_id', $id)->get();
        $remaining = $item_blance->where('type', '1')->sum('item_amount');
        $alloted = $item_blance->where('type', '0')->sum('item_amount');
        return [
            'remaining' => $remaining - $alloted,
            'alloted' => $alloted
        ];
    } else {
        return [
            'remaining' => $item_details_specifications->where('status', '0')->count(),
            'alloted' => $item_details_specifications->where('status', '1')->count()
        ];
    }
}
//get Employee information
function getEmployee($id)
{
    $employee = Employee::find($id);
    return $employee;
}
//single image store helper
// if (!function_exists('singleFileStore')) {
//     function singleFileStore($request, $max_id, $path)
//     {


//         if ($request->hasFile('image')) {
//         // if ($request->has('image')) {

//             $request->validate([
//                 'image' => 'required|mimes:jpg,png,jpeg'
//             ]);
//             //get the Image
//             $thumb = Image::make($request->image);

//             $thumb->resize(725, null, function ($constraint) {
//                 $constraint->aspectRatio();
//             });

//             $image_name = $max_id . '.jpg';

//             $image_path = $path . $image_name;
//             //store image and thumbnail in storage
//             $request->image->move($path, $image_name);

//             //db image storage
//             // $image_path = $path.$image_name;
//             // $thumb_path = $thumb_path;
//             $image = $image_path;
//             return $image;
//         } else {
//             return '';
//         }
//     }
// }

if (!function_exists('singleFileStore')) {
    function singleFileStore($request, $max_id, $path)
    {
        if ($request->hasFile('image')) {
            $request->validate([
                'image' => 'required|mimes:pdf,PDF'
            ]);

            $pdf_name = $max_id . rand() . '.pdf';
            $pdf_path = $path . $pdf_name;

            $request->image->move($path, $pdf_name);

            return $pdf_path;
        } else {
            return '';
        }
    }
}


// get remaining requested quantity (fece9 -> item_details -> requested_quantity )- alloted quantities
if (!function_exists('itemDetailsQuantities')) {
    function itemDetailsQuantities($fece9, $item_details_id)
    {
        $total = $fece9->itemDetails()->where('item_details_id', $item_details_id)->pluck('quantity')->first();
        $alloted = array_sum($fece9->fece5ItemDetails()->where('item_details_id', $item_details_id)->pluck('quantity')->toArray());
        return [
            'total' => $total,
            'alloted' => $alloted,
            'remaining' => $total - $alloted
        ];
    }
}

// get remaining requested quantity (fece9 -> item_details -> requested_quantity )- alloted quantities
if (!function_exists('arrayOfObjectsToNestedArray')) {
    function arrayOfObjectsToNestedArray($array)
    {
        $arr = [];
        foreach ($array as $index => $item) {
            array_push($arr, (array)$item);
        }
        return $arr;
    }
}

// get employees only related to auth user's department
if (!function_exists('getAuthEmployeeDepartment')) {
    function getAuthEmployeeDepartment()
    {
        $employee_id = auth()->user()->employee_id;
        $authUserDepartmentId = Employee::find($employee_id)->department_id;

        return $authUserDepartmentId;
    }
}
