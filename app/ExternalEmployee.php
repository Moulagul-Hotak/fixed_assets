<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExternalEmployee extends Model
{
    use LogsActivity;
    use SoftDeletes;
    protected $table = 'external_employee';
    protected $fillable = ['name_dr','name_en','father_name_dr','father_name_en','last_name','current_position_dr','current_position_en','external_department_id','created_at','updated_at','created_by','updated_by','deleted_at','deleted_by'];
    protected static $logAttributes = ['name_dr','name_en','father_name_dr','father_name_en','last_name','current_position_dr','current_position_en','external_department_id','created_at','updated_at','created_by','updated_by','deleted_at','deleted_by'];
    public function allotments() {
        return $this->hasMany('App\Allotment','receiver_employee_id','id')->where('employee_type',1);
    }
    public function employee_attachments() {
        return $this->hasMany('App\EmployeeAttachment','employee_id','id');
    }
    protected static function boot()
    {
        parent::boot();

        // create a event to happen on updating
        static::updating(function($table)  {
            $table->updated_by = Auth::user()->id ;
        });

        // create a event to happen on deleting
        static::deleting(function($table) {
            $table->deleted_by = Auth::user()->id ;
        });

        // create a event to happen on saving
        static::saving(function($table)  {
            $table->created_by = Auth::user()->id ;
        });
    }
    public function external_department() {
        return $this->belongsTo('App\ExternalDepartment');
    }
}
