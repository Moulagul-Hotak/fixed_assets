<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Traits\LogsActivity;

class ItemBalance extends Model
{
    //
    use LogsActivity;
    use SoftDeletes;
    protected $fillable = ['item_details_id', 'item_details_specifications_id', 'item_amount', 'type','created_by', 'updated_by', 'deleted_by'];

    protected static $logAttributes = ['item_details_id', 'item_details_specifications_id', 'item_amount', 'type','created_by', 'updated_by', 'deleted_by'];

    public function itemDetailsSpecifications() {
        return $this->belongsTo('App\ItemDetailsSpecifications', 'item_details_specifications_id', 'id');
    }

    public function allotment() {
        return $this->hasOne('App\Allotment','item_balances_id', 'id');
    }
   
    public static function boot() {
        parent::boot();

        // create a event to happen on updating
        static::updating(function($table)  {
            $table->updated_by = Auth::user()->id ;
        });

        // create a event to happen on deleting
        static::deleting(function($table)  {
            $table->deleted_by = Auth::user()->id ;
        });

        // create a event to happen on saving
        static::saving(function($table)  {
            $table->created_by = Auth::user()->id ;
        });
    }
}
