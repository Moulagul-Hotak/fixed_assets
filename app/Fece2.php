<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Fico7489\Laravel\Pivot\Traits\PivotEventTrait;

class Fece2 extends Model
{
    use LogsActivity;
    use SoftDeletes;
    use PivotEventTrait;

    protected $fillable = ['number', 'date',  'department_id','reciever','status','summary', 'file','meem7','reject_remark', 'created_by', 'updated_by', 'deleted_by'];
    protected static $logAttributes =['number', 'date',  'department_id','reciever','status','summary', 'file','meem7','reject_remark', 'created_by', 'updated_by', 'deleted_by'];


    public function department()
    {
        return $this->belongsTo('App\Department', 'department_id', 'id');
    }

    public static function boot() {
        parent::boot();
        static::pivotAttaching(function ($model, $relationName, $pivotIds, $pivotIdsAttributes) {
            if($relationName == 'itemDetails') {
                activity()
                ->performedOn($model)
                ->causedBy(auth()->user())
                ->withProperties($pivotIdsAttributes)
                ->log($relationName);}
        });
        // create a event to happen on updating
        static::updating(function($table)  {
            $table->updated_by = Auth::user()->id ;
        });

        // create a event to happen on deleting
        static::deleting(function($table) {
            $table->deleted_by = Auth::user()->id ;
        });

        // create a event to happen on saving
        static::saving(function($table)  {
            $table->created_by = Auth::user()->id ;
        });
    }
}
