<?php

namespace App\Exceptions;

use Exception;

class ItemDetailsSpecificationAlreadyAlloted extends Exception
{
    /**
     * Report or log an exception.
     *
     * @return void
     */
    public function report()
    {
        \Log::debug('Item Already Alloted');
    }
}
