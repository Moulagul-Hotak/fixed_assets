<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Contract extends Model
{
    //
    use LogsActivity;
    use SoftDeletes;
    protected $fillable = ['contract_type_id','identification_number','department_id','contractor_company_id','year','contract_amount','contract_discount','contract_discount_perce','start_date','end_date','contract_file','created_at','deleted_at','updated_at','created_by','deleted_by','updated_by'];
    protected static $logAttributes = ['contract_type_id','identification_number','department_id','contractor_company_id','year','contract_amount','contract_discount','contract_discount_perce','start_date','end_date','contract_file','created_at','deleted_at','updated_at','created_by','deleted_by','updated_by'];

    //
    public function department() {
        return $this->belongsTo('App\Department');
    }
    public function contractorCompany() {
        return $this->belongsTo('App\ContractorCompany');
    }

    public function contractItems() {
        return $this->hasMany('App\ContractItems');
    }
    public function contractType() {
        return $this->belongsTo('App\ContractType');
    }
    public static function boot() {
        parent::boot();

        // create a event to happen on updating
        static::updating(function($table)  {
            $table->updated_by = Auth::user()->id ;
        });

        // create a event to happen on deleting
        static::deleting(function($table)  {
            $table->deleted_by = Auth::user()->id ;
        });

        // create a event to happen on saving
        static::saving(function($table)  {
            $table->created_by = Auth::user()->id ;
        });
    }
    
}
